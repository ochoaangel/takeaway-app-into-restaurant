import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrecioPipe } from './precio.pipe';
import { TimeDiffPipe } from './time-diff.pipe';
import { To12hPipe } from './to12h.pipe';



@NgModule({
  declarations: [PrecioPipe, TimeDiffPipe, To12hPipe],
  exports: [PrecioPipe, TimeDiffPipe, To12hPipe],
  imports: [
    CommonModule
  ]
})
export class MyPipesModule { }

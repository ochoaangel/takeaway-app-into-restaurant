import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'underscore';

@Pipe({
  name: 'timeDiff'
})
export class TimeDiffPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    // console.log('value', value);
    // console.log('args', args);

    let ft = 'YYYY-MM-DD HH:mm:ss'

    let antes = moment(value, ft);
    // let despues = moment(args[0], ft);


    let despues;
    if (args[0]) {
      // si recibo un argumento ese seria el final
      despues = moment(args[0], ft);
    } else {
      // cuando NO recibo argumento el final seria el momento actual
      despues = moment();
    }

    // realizo el calculo y obtengo detalles
    let d = moment.duration(despues.diff(antes))['_data']

    // console.log('duración: ', d);

    let end = '';

    if (
      d.years < 1 &&
      d.months < 1 &&
      d.days < 1 &&
      d.hours < 1 &&
      d.minutes < 1
      // d.seconds < 1
    ) {
      end = 'Menos de 1 Minuto';
    }

    if (d.years > 1) {
      end = ` ${d.years} Años.`;
    } else if (d.years === 1) {
      end = ` ${d.years} Año.`;

    } else if (d.months > 1) {
      end = ` ${d.months} Meses.`;
    } else if (d.months === 1) {
      end = ` ${d.months} Mes.`;

    } else if (d.days > 1) {
      end = ` ${d.days} Días.`;
    } else if (d.days === 1) {
      end = ` ${d.days} Día.`;

    } else if (d.hours > 1) {
      end = ` ${d.hours} Horas.`;
    } else if (d.hours === 1) {
      end = ` ${d.hours} Hora.`;

    } else if (d.minutes > 1) {
      end = ` ${d.minutes} Minutos.`;
    } else if (d.minutes === 1) {
      end = ` ${d.minutes} Minuto.`;
    }




    return end;
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'precio'
})
export class PrecioPipe implements PipeTransform {

  transform(value: number, ...args: any[]): string {
    // console.log('args', args);
    let end = '';
    const miPrecio = value;
    let entero = '';
    let decimal = '';
    let separadorDecimal = ',';

    const index = miPrecio.toString().indexOf('.');

    if (index >= 0) {
      entero = miPrecio.toString().slice(0, index);
      decimal = miPrecio.toString().slice(index + 1, index + 3);
      decimal = decimal.length === 1 ? decimal + '0' : decimal;
    } else {
      //caso no tiene decimal
      entero = miPrecio.toString();
      separadorDecimal = '';
    }


    switch (args[0]) {

      case 'entero':
      case 'integer':
      case 'int':
        end = entero;
        break;

      case 'decimal':
        end = decimal;
        break;

      case 'separadorDecimal':
        end = separadorDecimal;
        break;

      default:
        end = 'ERRORpipePrecio';
        break;
    }

    return end;
  }

}

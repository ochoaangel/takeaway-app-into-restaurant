import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'underscore';

@Pipe({
  name: 'to12h'
})
export class To12hPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    let end;

    let antes = moment(value, 'HH:mm');
    end = antes.format('hh:mm a');

    return end;
  }

}

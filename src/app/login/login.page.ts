import { MyVarService } from './../services/my-var.service';
import { Component, OnInit } from '@angular/core';
import { ApiCallService } from '../services/api-call.service';
import { Router } from '@angular/router';
import { MyFunctionsService } from '../services/my-functions.service';
import { MyStorageService } from '../services/my-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loading = 0;

  constructor(
    private apiCall: ApiCallService,
    private myV: MyVarService,
    private router: Router,
    private myF: MyFunctionsService,
    private myS: MyStorageService


  ) { }

  user = { email: '', password: '' };
  ngOnInit() {

    // this.myF.printWithEpson();

    // console.log('this.user', this.user);
    // this.apiCall.logIn(this.user).subscribe(resp => {


    //   if (resp && resp.access_token) {
    //     this.myV.access_token = resp.access_token;
    //     console.log('LOGEADO con acces_token en this.myV.access_token: ', this.myV.access_token);
    //     this.router.navigateByUrl('/tabs/pedidos/pedidos');
    //   } else {
    //     this.myF.showAlertOk('Notificación', 'info', 'Datos incorrectos, intente nuevamente..');
    //   }
    // });

    this.user = { email: '', password: '' };

  }


  ionViewWillEnter() {
    this.user = { email: '', password: '' };
    // console.log('verificando si hay token');

    if (this.myV.access_token) {
      // console.log('verificaDO, si hay token');
      this.router.navigateByUrl('/tabs/inicio');
    } else {

      this.myS.get('user').subscribe(resp => {
        if (resp && resp.access_token) {
          this.myV.access_token = resp.access_token;
          this.myV.user = resp;
          console.log('LOGEADO con acces_token en this.myV.access_token: ', this.myV.access_token);
          this.router.navigateByUrl('/tabs/inicio');
        }
      })


    }

  }

  btnEntrar() {
    this.loading++;


    // evitando error 401
    let timeInit = true;
    setTimeout(() => {
      if (timeInit) {
        this.loading--;
        timeInit = false;
        this.myF.showAlertOk('Notificación', 'info', 'Datos incorrectos, intente nuevamente..');
      }
    }, 3000);

    this.apiCall.logIn(this.user).subscribe(resp => {
      this.loading--;
      timeInit = false;

      if (resp && resp.access_token) {
        this.myV.access_token = resp.access_token;
        this.myV.user = resp;
        console.log('LOGEADO con acces_token en this.myV.access_token: ', this.myV.access_token);

        this.myS.set('user', resp).subscribe(respSetUser => {
          if (respSetUser) {
            this.router.navigateByUrl('/tabs/inicio');
          }
        });
      } else {
        this.myF.showAlertOk('Notificación', 'info', 'Datos incorrectos, intente nuevamente..');
      }
    });
  }





}

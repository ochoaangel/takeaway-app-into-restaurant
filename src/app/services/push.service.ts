import { Injectable, EventEmitter } from '@angular/core';
import { OneSignal, OSNotification, OSNotificationPayload } from '@ionic-native/onesignal/ngx';
import { MyVarService } from './my-var.service';
import { Platform } from '@ionic/angular';
import { MyFunctionsService } from './my-functions.service';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class PushService {


  constructor(
    private oneSignal: OneSignal,
    private myV: MyVarService,
    private myF: MyFunctionsService,
    private platform: Platform,
    private router: Router,
  ) { }

  async configuracionInicial() {

    let startInit;

    if (this.platform.is('ios') || this.platform.is('ipad') || this.platform.is('iphone')) {
      // caso IOS
      const iosSettings = { kOSSettingsKeyAutoPrompt: true, kOSSettingsKeyInAppLaunchURL: false };

      startInit = this.oneSignal
        .startInit(this.myV.OneSignalAppID, this.myV.FirebaseSenderID)
        .iOSSettings(iosSettings);
    } else {
      // caso Android
      startInit = this.oneSignal
        .startInit(this.myV.OneSignalAppID, this.myV.FirebaseSenderID);
    }

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);



    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
      // do something when notification is received
      // console.log('>>>>>>>Notificación Recibida', noti);

      switch (noti.payload.additionalData.action) {
        /////////////////////////////////////////////////////////////////////////////  
        case 'redirect':
          // caso de redirecciones
          switch (noti.payload.additionalData.url) {
            case 'pedidos':

              switch (this.router.url) {

                case '/tabs/pedidos/pedidos':
                  console.log('Caso de Notificación Recibida, redirigida a pedidos, Actualizo pedidos');
                  this.myF.showToast('Nuevo pedido actualizado en el tablero', 'checkmark-done');
                  this.myV.updatePedidosNow.next(true);
                  break;

                default:
                  console.log('Caso de Notificación Recibida, redirigida a pedidos, no paurtada acciones');
                  this.myF.showToast('Ha llegado un nuevo pedido', 'hourglass');
                  break;

              } // fin switch url en la que estoy

              break;  // fin case 'pedidos'

            default:
              console.warn('Redirección Recibida por ONESIGNAL no reconocida');
              break;

          }   // fin switch pestanas a redireccionar

          break;   // fin case 'redirect'

        /////////////////////////////////////////////////////////////////////////////  
        case 'otherAction':
          break;
        /////////////////////////////////////////////////////////////////////////////  
        case 'CALL-SERVICE':
          if (noti.payload.additionalData.data.nombre) {
            this.myF.showAlertOk(`Dentro del Local`, 'info',
              `La mesa: ${noti.payload.additionalData.data.nombre} ${noti.payload.additionalData.data.group ? noti.payload.additionalData.data.group : ''}, requiere su atención`);
          }

          break;

        /////////////////////////////////////////////////////////////////////////////  
        default:
          console.warn('Acción Recibida por ONESIGNAL no reconocida');
          break;
      } // fin tipos de acciones 

    });

    this.oneSignal.handleNotificationOpened().subscribe(noti => {


      switch (noti.notification.payload.additionalData.action) {
        /////////////////////////////////////////////////////////////////////////////  
        case 'redirect':
          // caso de redirecciones
          switch (noti.notification.payload.additionalData.url) {
            case 'pedidos':

              switch (this.router.url) {

                case '/tabs/pedidos/pedidos':
                  console.log('Caso de Notificación Abierta, redirigida a pedidos, Actualizo pedidos');
                  this.myV.updatePedidosNow.next(true);
                  break;

                default:
                  console.log('Caso de Notificación Abierta, redirigida a pedidos, Redirijo a pedidos.');
                  this.router.navigateByUrl('/tabs/pedidos');
                  break;

              } // fin switch url en la que estoy

              break;  // fin case 'pedidos'

            default:
              console.warn('Redirección Recibida por ONESIGNAL no reconocida');
              break;

          }   // fin switch pestanas a redireccionar

          break;   // fin case 'redirect'

        /////////////////////////////////////////////////////////////////////////////  
        case 'otherAction':
          break;

        /////////////////////////////////////////////////////////////////////////////  
        default:
          console.warn('Acción Recibida por ONESIGNAL no reconocida');
          break;
      } // fin tipos de acciones 



      if (this.router.url.includes('pedidos')) {
        // actualizar pedidos
        console.log('Actualizando pedidos');
        // this.myV.updatePedidosNow = true;
        this.myV.updatePedidosNow.next(true);

      } else {
        // redirigir
        console.log('Redirigido a pedidos');
        this.router.navigateByUrl('/tabs/pedidos');
      }
    });

    this.oneSignal.endInit();

    try {
      console.log('ONESIGNAL >> Entrando a Try');
      const resp = await this.oneSignal.getIds();

      console.log('ONESIGNAL >> UID Recibido: ' + resp.userId);

      this.myV.DvDeviceUid = resp.userId || '';
      if (!this.myV.DvDeviceUid) {
        console.warn('ONESIGNAL >> No hay UID asignado');
      } else {
        console.log('ONESIGNAL UID ASIGNADO EN INICIO >> ' + this.myV.DvDeviceUid);
      }

    } catch (error) {
      console.error('ONESIGNAL >> Error al intentar getIds(): ' + JSON.stringify(error));
    }

  }

}

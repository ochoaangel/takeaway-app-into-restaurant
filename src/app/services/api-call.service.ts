import { ApiOrder, ApiTurn2, ApiTurn1, ApiTurn } from './../Interfaces/interfaces';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MyVarService } from './my-var.service';
import { ApiOrderLive, Order } from '../Interfaces/interfaces';

@Injectable({
  providedIn: 'root',
})
export class ApiCallService {
  baseUrl;

  constructor(private http: HttpClient, private myV: MyVarService) {
    // this.baseUrl = 'https://takeaway-cool-app-backend.herokuapp.com/api/v1.0';
    // this.baseUrl = 'http://localhost:3000/api/v1.0';
    this.baseUrl = 'https://backend.takeaway.cool/api/v1';
  }


  ////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////  GET  ///////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////

  // cartCount(): Observable<any> {
  //   let url = '/cart?asCount=1';
  //   let urlFinal = this.baseUrl + url;
  //   return this.http.get<any>(urlFinal, {});
  // }

  ////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////  POST  ///////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  logIn(params): Observable<any> {
    let url = '/auth/login';
    let urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, params);
  }

  logOut(params): Observable<any> {
    let url = '/auth/logout';
    let urlFinal = this.baseUrl + url;
    return this.http.post<any>(urlFinal, params);
  }

  ordersLive(): Observable<any> {
    let url = '/delivery/orders/live';
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiOrderLive>(urlFinal, {});
  }


  ////////////////////////////////////////////////////////////////////////////////
  // dvOrders, Cambiar de estados
  orderAccept(id, params): Observable<any> {
    let url;
    // if (this.myV.dispatch_mode === 'date') {
    //   url = `/delivery/orders/${id}/accept/refresh-dispatch-date`;
    // } else {
    url = `/delivery/orders/${id}/accept`;
    // }
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiOrder>(urlFinal, params);
  }

  orderPrepareOnlyInDispashdateDate(id, params): Observable<any> {
    let url;
    // if (this.myV.dispatch_mode === 'date') {
      url = `/delivery/orders/${id}/accept/refresh-dispatch-date`;
    // } else {
    //   url = `/delivery/orders/${id}/accept`;
    // }
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiOrder>(urlFinal, params);
  }

  orderFinish(id): Observable<any> {
    let url = `/delivery/orders/${id}/finish`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiOrder>(urlFinal, {});
  }
  orderDispatch(id): Observable<any> {
    let url = `/delivery/orders/${id}/dispatch`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiOrder>(urlFinal, {});
  }
  orderCancel(id, params): Observable<any> {
    let url = `/delivery/orders/${id}/cancel`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiOrder>(urlFinal, params);
  }

  ////////////////////////////////////////////////////////////////////////////////
  // control

  /**
   * @param params
   * ► {list: "zones"} ó
   * ► {list: "categories"} ó
   * ► {list: "payMethods"} ó
   * ► {list: "products" ,dvcategory_id:"1" } =>  dvcategory_id es Opcional, pero que permite obtener los productos segun categoria
   */
  controlsList(params): Observable<any> {
    let url = `/delivery/controls/lists`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiOrder>(urlFinal, params);
  }

  /**
   * @param params
   * ► OPCIONES A: Activa | I: Inactiva | N: Apagada Aplica para todos los elementos Switchables'
   * ► ZONAS {"change": "zone", "id": 1, "next": "I" }
   * ► PRODUCTOS {"change": "product","id": 1,"next": "A" }
   * ► CATECORIAS {"change": "product","id": 1,"next": "N" }
   * ► METODOS DE PAGO {"change": "product","id": 1,"next": "N" }
   * 
   */
  controlsSwitch(params): Observable<any> {
    let url = `/delivery/controls/switch`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiOrder>(urlFinal, params);
  }

  ////////////////////////////////////////////////////////////////////////////////////
  // turns

  /**
   * 
   * @param params  {tipo:'L'}  ó 'D'   para horario Local y Delivery
   */
  turnsSee(params): Observable<any> {
    let url = `/delivery/controls/turns/see`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiTurn2>(urlFinal, params);
  }

  /**
   * 
   * @param params {"dia": 4,"hora_inicio": "20:00","hora_fin": "23:15","tipo": "D"}  D►Delivery , L►Local, 0►Domingo
   * 
   */
  turnsAdd(params): Observable<any> {
    let url = `/delivery/controls/turns/add`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiTurn>(urlFinal, params);
  }


  // preguntarrrrrrr
  turnsRem(params): Observable<any> {
    let url = `/delivery/controls/turns/rem`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiOrder>(urlFinal, params);
  }

  /**
   * 
   * @param params {"dvrestturn_id": 2,"next": "0"}  0►apagar  1►Encender
   */
  turnsSwitch(params): Observable<any> {
    let url = `/delivery/controls/turns/switch`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiTurn>(urlFinal, params);
  }

  //////////////////////////////////////////////////////////////

  buscarHistoricoPedido(params): Observable<any> {
    let url = `/delivery/orders/browse`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiTurn>(urlFinal, params);
  }

  verDetalleHistoriPedido(params): Observable<any> {
    let url = `/delivery/orders/view/` + params.id;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiTurn>(urlFinal, params);
  }

  home(): Observable<any> {
    let url = `/delivery/home`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiTurn>(urlFinal, {});
  }

  initTransit(): Observable<any> {
    let url = `/delivery/orders/browse/in-transit`;
    let urlFinal = this.baseUrl + url;
    return this.http.post<ApiTurn>(urlFinal, {});
  }


}

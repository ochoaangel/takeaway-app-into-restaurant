import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Observable, Subscriber } from 'rxjs';
import * as _ from 'underscore';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class MyStorageService {

  constructor(
    public platform: Platform,
    private nativeStorage: NativeStorage
  ) { }


  set(keyName, value): Observable<boolean> {
    return new Observable((observer: Subscriber<boolean>) => {
      if ((this.platform.is('android') || this.platform.is('ios')) && this.platform.is('cordova')) {
        // caso dispositivo
        try {
          this.nativeStorage.setItem(keyName, value).then(
            () => {
              observer.next(true);
              observer.complete();
            }, err => {
              observer.next(false);
              observer.complete();
            }
          );
        } catch (reason) {
          console.warn(reason);
          observer.next(false);
          observer.complete();
        }

      } else {
        // caso web
        console.log('Caso Web');
        localStorage.setItem(keyName, JSON.stringify(value));
        observer.next(true);
        observer.complete();
      }
    });
  } // fin set


  get(keyName): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      if ((this.platform.is('android') || this.platform.is('ios')) && this.platform.is('cordova')) {
        // caso dispositivo
        try {
          this.nativeStorage.getItem(keyName)
            .then(
              data => {
                observer.next(data);
                observer.complete();
              }, err => {
                observer.next();
                observer.complete();
              }
            );
        } catch (reason) {
          console.warn(reason);
          observer.next();
          observer.complete();
        }

      } else {
        // caso web
        observer.next(JSON.parse(localStorage.getItem(keyName)));
        observer.complete();
      }
    });
  } // fin get


  remove(keyName): Observable<boolean> {
    return new Observable((observer: Subscriber<boolean>) => {
      if ((this.platform.is('android') || this.platform.is('ios')) && this.platform.is('cordova')) {
        // caso dispositivo
        try {
          this.nativeStorage.remove(keyName)
            .then(
              data => {
                observer.next(true);
                observer.complete();
              }, err => {
                observer.next(false);
                observer.complete();
              }
            );
        } catch (reason) {
          console.warn(reason);
          observer.next(false);
          observer.complete();
        }

      } else {
        // caso 
        localStorage.removeItem(keyName);
        observer.next(true);
        observer.complete();
      }
    });
  } // fin get




  // gps_chequearSiTenemosPermisoGps(): Observable<boolean> {
  //   return new Observable((observer: Subscriber<boolean>) => {
  //     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
  //       result => {
  //         if (result.hasPermission) {
  //           // If having permission show 'Turn On GPS' dialogue
  //           // this.askToTurnOnGPS();
  //           observer.next(true);
  //           observer.complete();

  //         } else {
  //           // If not having permission ask for permission
  //           // this.requestGPSPermission();
  //           // observer.error(false)
  //           observer.next(false);
  //           observer.complete();
  //         }
  //       },
  //       err => {
  //         // alert(err);
  //         observer.next(false);
  //         observer.complete();
  //         // observer.error(false)
  //       });
  //   });
  // } // fin d








  // isDevice(): Observable




  // async isDevice(): Promise<boolean> {
  //   if (this.platform.is('android') || this.platform.is('ios')) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }




  // // set a key/value
  // async set(key: string, value: any): Promise<any> {
  //   if (await this.isDevice()) {
  //     try {
  //       const result = await this.nativeStorage.setItem(key, value);
  //       console.log('set string in storage: ' + result);
  //       return true;
  //     } catch (reason) {
  //       console.log(reason);
  //       return false;
  //     }
  //   } else {
  //     console.log('es Web desde SET')
  //   }
  // }




  // // to get a key/value pair
  // async get(key: string): Promise<any> {
  //   try {
  //     const result = await this.nativeStorage.getItem(key);
  //     console.log('storageGET: ' + key + ': ' + result);
  //     if (result != null) {
  //       return result;
  //     }
  //     return null;
  //   } catch (reason) {
  //     console.log(reason);
  //     return null;
  //   }
  // }

  // // set a key/value object
  // async setObject(key: string, object: Object) {
  //   try {
  //     const result = await this.nativeStorage.setItem(key, JSON.stringify(object));
  //     console.log('set Object in storage: ' + result);
  //     return true;
  //   } catch (reason) {
  //     console.log(reason);
  //     return false;
  //   }
  // }

  // // get a key/value object
  // async getObject(key: string): Promise<any> {
  //   try {
  //     const result = await this.nativeStorage.getItem(key);
  //     if (result != null) {
  //       return JSON.parse(result);
  //     }
  //     return null;
  //   } catch (reason) {
  //     console.log(reason);
  //     return null;
  //   }
  // }

  // // remove a single key value:
  // remove(key: string) {
  //   this.nativeStorage.remove(key);
  // }

  // //  delete all data from your application:
  // clear() {
  //   this.nativeStorage.clear();
  // }


}



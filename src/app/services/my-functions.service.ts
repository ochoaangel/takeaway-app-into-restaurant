import { Order } from './../Interfaces/interfaces';
import { filter } from 'rxjs/operators';
import { ApiCallService } from './api-call.service';
import { Injectable } from '@angular/core';
import { MyVarService } from './my-var.service';
import { AlertController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Observable, Subscriber } from 'rxjs';

import * as moment from 'moment';
import 'moment/min/locales';
import 'moment-timezone';
import { NULL_EXPR } from '@angular/compiler/src/output/output_ast';


declare const epson: any;

@Injectable({
  providedIn: 'root'
})
export class MyFunctionsService {

  ePosDev = new epson.ePOSDevice();
  myPrinter = null;

  constructor(
    public myV: MyVarService,
    public router: Router,
    private apiCall: ApiCallService,
    public alertCtrl: AlertController,
    public toastController: ToastController,
  ) {
    moment.locale('es');
    console.log('dsd Constructor Funciones1', moment().format('LLLL'));
    console.log('dsd Constructor Funciones5', moment.utc().format('LLLL'));
  }


  async showAlertOk(titulo: string, icon: string, mensajeHTML: string, redirectTo?: string) {

    let icono;
    switch (icon) {
      case 'info':
      case 'inf':
        icono = 'information-circle-outline';
        break;

      case 'done':
        icono = 'checkmark-done-circle-outline';
        break;

      default:
        icono = icon;
        break;
    }


    const alert = await this.alertCtrl.create({
      header: titulo,
      mode: 'ios',
      message: `</br><ion-icon name="${icono}"></ion-icon><br>${mensajeHTML}`,
      buttons: [
        {
          text: 'OK',
          role: 'OK',
          handler: (blah) => {
            console.log('Confirm OK: blah');
            if (redirectTo) {
              this.router.navigateByUrl(redirectTo);
            }
          }
        }
      ]
    });
    await alert.present();
  }


  imprimirPrueba(impresora): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      const host = impresora.name;
      const ip = impresora.ip;
      console.log('EPSON DRIVER: Intentando conectar con el Host: ' + host + ' en la IP: ' + ip);
      let printer = null;
      const EPSON = new epson.ePOSDevice();

      EPSON.connect(ip, '8008', (EPSON_state) => {

        console.log('EPSON DRIVER: La impresora ha respondido...');
        console.log([EPSON_state, EPSON]);
        if (EPSON_state === 'OK' || EPSON_state === 'SSL_CONNECT_OK') {
          console.log('EPSON DRIVER: La impresora aceptó la conexion, se crea un Dispositivo de Impresión');
          EPSON.createDevice('local_printer', EPSON.DEVICE_TYPE_PRINTER, { crypto: true, buffer: false }, (DEVICE, CODE) => {
            if (CODE === 'OK') {
              printer = DEVICE;
              console.log('EPSON DRIVER: Dispositivo preparado, se exporta la impresora al Callback');
              ///////////////////////////////////////////////////////////////////
              ///////////////////////////////////////////////////////////////////
              printer.addFeedLine(1);
              printer.addTextSize(2, 1);
              printer.addText(`Probando`);
              printer.addFeedLine(1);
              printer.addTextSize(2, 1);
              printer.addText(`Impresora:`);
              printer.addFeedLine(1);
              printer.addTextSize(2, 1);
              printer.addText('  ' + impresora.name);
              printer.addFeedLine(1);
              printer.addTextSize(2, 1);
              printer.addText(`con ip: ${impresora.ip}`);
              printer.addFeedLine(1);

              // Finish Printing
              printer.addCut(printer.CUT_FEED);
              printer.send();

              EPSON.disconnect();

              observer.next({ status: true, message: '' });
              observer.complete();

            } else {
              console.error('Error al Imprimir: ' + CODE);
              EPSON.disconnect();
              observer.next({ status: false, message: 'Error al Imprimir: ' + CODE });
              observer.complete();
            }
          });
        }
        else {
          console.error('Error al conectar con la Impresora: ' + EPSON_state);
          EPSON.disconnect();
          observer.next({ status: false, message: 'Error al conectar con la Impresora: ' + EPSON_state });
          observer.complete();
        }
      });

    });
  } // fin set

  imprimirAdmin(ordenCompleta, impresora): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      const host = impresora.name;
      const ip = impresora.ip;
      const pedido = ordenCompleta;
      console.log('EPSON DRIVER: Intentando conectar con el Host: ' + host + ' en la IP: ' + ip);
      let printer = null;
      const EPSON = new epson.ePOSDevice();

      EPSON.connect(ip, '8008', (EPSON_state) => {

        console.log('EPSON DRIVER: La impresora ha respondido...');
        console.log([EPSON_state, EPSON]);
        if (EPSON_state === 'OK' || EPSON_state === 'SSL_CONNECT_OK') {
          console.log('EPSON DRIVER: La impresora aceptó la conexion, se crea un Dispositivo de Impresión');
          EPSON.createDevice('local_printer', EPSON.DEVICE_TYPE_PRINTER, { crypto: true, buffer: false }, (DEVICE, CODE) => {
            if (CODE === 'OK') {
              printer = DEVICE;
              console.log('EPSON DRIVER: Dispositivo preparado, se exporta la impresora al Callback');
              this.rutinaImpresionAdmin(printer, pedido);
              printer.send();

              EPSON.disconnect();

              observer.next({ status: true, message: '' });
              observer.complete();

              // this.showAlertOk('Notificación', 'done', 'Exitoooooooooooo');
            } else {
              console.error('Error al Imprimir: ' + CODE);
              EPSON.disconnect();
              observer.next({ status: false, message: 'Error al Imprimir: ' + CODE });
              observer.complete();
            }
          });
        }
        else {
          console.error('Error al conectar con la Impresora: ' + EPSON_state);
          EPSON.disconnect();
          observer.next({ status: false, message: 'Error al conectar con la Impresora: ' + EPSON_state });
          observer.complete();
        }
      });

    });
  } // fin set

  imprimirCocina(ordenCompleta, impresora): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      const host = impresora.name;
      const ip = impresora.ip;
      const pedido = ordenCompleta;
      console.log('EPSON DRIVER: Intentando conectar con el Host: ' + host + ' en la IP: ' + ip);
      let printer = null;
      const EPSON = new epson.ePOSDevice();

      EPSON.connect(ip, '8008', (EPSON_state) => {

        console.log('EPSON DRIVER: La impresora ha respondido...');
        console.log([EPSON_state, EPSON]);
        if (EPSON_state === 'OK' || EPSON_state === 'SSL_CONNECT_OK') {
          console.log('EPSON DRIVER: La impresora aceptó la conexion, se crea un Dispositivo de Impresión');
          EPSON.createDevice('local_printer', EPSON.DEVICE_TYPE_PRINTER, { crypto: true, buffer: false }, (DEVICE, CODE) => {
            if (CODE === 'OK') {
              printer = DEVICE;
              console.log('EPSON DRIVER: Dispositivo preparado, se exporta la impresora al Callback');
              this.rutinaImpresionCocina(printer, pedido);
              printer.send();

              EPSON.disconnect();

              observer.next({ status: true, message: '' });
              observer.complete();

              // this.showAlertOk('Notificación', 'done', 'Exitoooooooooooo');
            } else {
              console.error('Error al Imprimir: ' + CODE);
              EPSON.disconnect();
              observer.next({ status: false, message: 'Error al Imprimir: ' + CODE });
              observer.complete();
            }
          });
        }
        else {
          console.error('Error al conectar con la Impresora: ' + EPSON_state);
          EPSON.disconnect();
          observer.next({ status: false, message: 'Error al conectar con la Impresora: ' + EPSON_state });
          observer.complete();
        }
      });

    });
  } // fin set

  imprimirAdminAndCocina(ordenCompleta, impresora, onceMoreAdminTicket = false): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      const host = impresora.name;
      const ip = impresora.ip;
      const pedido = ordenCompleta;
      console.log('EPSON DRIVER: Intentando conectar con el Host: ' + host + ' en la IP: ' + ip);
      let printer = null;
      const EPSON = new epson.ePOSDevice();

      EPSON.connect(ip, '8008', (EPSON_state) => {

        console.log('EPSON DRIVER: La impresora ha respondido...');
        console.log([EPSON_state, EPSON]);
        if (EPSON_state === 'OK' || EPSON_state === 'SSL_CONNECT_OK') {
          console.log('EPSON DRIVER: La impresora aceptó la conexion, se crea un Dispositivo de Impresión');
          EPSON.createDevice('local_printer', EPSON.DEVICE_TYPE_PRINTER, { crypto: true, buffer: false }, (DEVICE, CODE) => {
            if (CODE === 'OK') {

              printer = DEVICE;
              this.rutinaImpresionAdmin(printer, pedido);
              if (onceMoreAdminTicket) { this.rutinaImpresionAdmin(printer, pedido); }
              this.rutinaImpresionCocina(printer, pedido);
              printer.send();

              EPSON.disconnect();
              observer.next({ status: true, message: '' });
              observer.complete();
              // this.showAlertOk('Notificación', 'done', 'Exitoooooooooooo');
            } else {
              console.error('Error al Imprimir: ' + CODE);
              EPSON.disconnect();
              observer.next({ status: false, message: 'Error al Imprimir: ' + CODE });
              observer.complete();
            }
          });
        }
        else {
          console.error('Error al conectar con la Impresora: ' + EPSON_state);
          EPSON.disconnect();
          observer.next({ status: false, message: 'Error al conectar con la Impresora: ' + EPSON_state });
          observer.complete();
        }
      });

    });
  } // fin set

  // --------------------------------------------
  // RUTINAS DE IMPRESION (MODELO DEL TICKET)
  // --------------------------------------------

  rutinaImpresionAdmin(printer, pedido: Order){
    printer.addFeedLine(1);
    printer.addTextAlign(printer.ALIGN_CENTER);
    printer.addTextSize(3, 2);
    printer.addText(`${pedido.order_code} - ${pedido.pagado ? 'Pagado' : 'No Pagado'} `);
    printer.addTextSize(1, 1);
    printer.addFeedLine(1);

    printer.addFeedLine(1);
    printer.addTextAlign(printer.ALIGN_LEFT);
    printer.addTextSize(1, 1);

    printer.addText(`ID: #${pedido.id}\n`);
    printer.addText(`Recibido el: ${moment(pedido.created_at).format('dddd, MMMM Do YYYY, H:mm')} \n`);
    printer.addText(`Razón Social: ${pedido.client ? pedido.client.nombre : 'Cliente Particular'} \n`);
    if (pedido.client?.tel){
      printer.addText(`Teléfono del Cliente: ${pedido.client.tel} \n`);
    }

    if (pedido.zone) { printer.addText(`Dirección: ${pedido.zone.nombre} (${pedido.zone.code}) ${pedido.dir}, ${pedido.dir2} \n`); }
    else if (pedido.board) { printer.addText(`Mesa: ${pedido.board.nombre} \n`); }
    else { printer.addText('Dirección: Dentro del Local\n'); }

    printer.addText(`Método de Pago: ${pedido.pay_method.nombre} \n`);

    if (pedido.pay_method.driver.type === 'effective' && pedido.ref_transaccion) {
      printer.addText(`Monto de vuelta: ${ pedido.ref_transaccion } \n`);
    }

    printer.addText(''.padStart(48, '.'));
    printer.addFeedLine(1);

    pedido.items.forEach(item => {
      printer.addText(item.item_name.length > 30 ? item.item_name.substring(0, 30) : item.item_name.padEnd(30));

      printer.addText('  ');
      printer.addText(item.cantidad ? ('x' + item.cantidad).padStart(6) : ''.padStart(6));

      printer.addText('  ');
      printer.addText((item.subtotal.toLocaleString('es') + ' €').padStart(8));
      printer.addText('\n');
    });

    printer.addText('--------'.padStart(48));
    printer.addText('TOTAL COMPRA');
    printer.addTextSize(2, 1);
    printer.addText((pedido.total.toLocaleString('es') + ' €').padStart(18, '. '));

    printer.addFeedPosition(printer.FEED_PEELING);
    printer.addFeedLine(3);

    // Finish Printing
    printer.addCut(printer.CUT_FEED);
  }

  rutinaImpresionCocina(printer, pedido){
    printer.addFeedLine(1);
    printer.addTextAlign(printer.ALIGN_CENTER);
    printer.addTextSize(3, 2);
    printer.addText(pedido.order_code);
    printer.addText(`\n\n`);
    printer.addTextSize(2, 1);

    if ( pedido.dispatch_date ){
      printer.addText(`Entregar al: ${ moment(pedido.dispatch_date).format('dddd') } \n`);
      printer.addText(`${ moment(pedido.dispatch_date).format('D [de] MMMM, YYYY') } \n`);
      printer.addText(`a las: ${ moment(pedido.dispatch_date).format('H:mm') } \n`);
    }
    else {
      printer.addText(`Tiempo: ${pedido.tiempo} min \n`);
    }

    printer.addTextSize(1, 1);

    printer.addFeedLine(1);
    printer.addTextAlign(printer.ALIGN_LEFT);
    printer.addText(`Recibido el: ${moment(pedido.created_at).format('dddd, MMMM Do YYYY, H:mm')}\n`);

    const nombreCliente = pedido.client ? pedido.client.nombre : (pedido.nombre_cliente ? pedido.nombre_cliente : 'Cliente Particular');
    printer.addText(`Razón Social: ${nombreCliente}\n`);
    printer.addFeedLine(1);

    if (pedido.zone) {
      printer.addFeedLine(1);
      printer.addTextAlign(printer.ALIGN_CENTER);
      printer.addTextSize(1, 1);
      printer.addText(`Para enviar a: \n`);
      printer.addTextSize(2, 2);
      printer.addText(`${pedido.zone.nombre}\n`);
    }

    else if ( pedido.zone == null && pedido.via === 'app'){
      printer.addFeedLine(1);
      printer.addTextAlign(printer.ALIGN_CENTER);
      printer.addText(`Este pedido es: \n`);
      printer.addTextSize(2, 2);
      printer.addText(`PARA RECOGIDA\n`);
    }

    // tslint:disable-next-line: align
    if (pedido.board || pedido.nota) {
      printer.addFeedLine(1);
      printer.addTextSize(1, 1);
      printer.addTextAlign(printer.ALIGN_CENTER);
      printer.addText(`--------------------------------\n`);

      if (pedido.board) {
        printer.addFeedLine(1);
        printer.addTextSize(1, 1);
        printer.addTextAlign(printer.ALIGN_CENTER);
        printer.addText(`Para comer aquí`);
        printer.addFeedLine(1);

        printer.addTextSize(3, 3);
        printer.addTextAlign(printer.ALIGN_CENTER);
        printer.addText(`${pedido.board.nombre}\n`);

        if (pedido.board.group){
          printer.addTextSize(2, 2);
          printer.addText(`${pedido.board.group}\n`);
        }
      }

      if (pedido.nota) {
        printer.addFeedLine(1);
        printer.addTextSize(2, 1);
        printer.addTextAlign(printer.ALIGN_CENTER);
        printer.addText(`Nota\n`);
        printer.addTextSize(2, 2);
        printer.addText(`${pedido.nota}\n`);

        printer.addFeedLine(1);
      }

      printer.addTextSize(1, 1);
      printer.addTextAlign(printer.ALIGN_CENTER);
      printer.addText(`--------------------------------\n`);
      printer.addTextAlign(printer.ALIGN_LEFT);
      printer.addFeedLine(1);
    }

    // Impresion de Productos
    pedido.items.forEach(item => {

      // Evitar imprimir items que no son productos
      if (!item.dvproduct_id) { return; }

      printer.addTextSize(3, 2);
      printer.addTextAlign(printer.ALIGN_LEFT);
      printer.addText(` ${item.cantidad}x `);

      printer.addTextSize(2, 2);
      printer.addText(`${item.item_name}\n`);

      if (item.nota_variaciones) {
        printer.addTextSize(2, 1);
        printer.addText(`${item.nota_variaciones}\n`);
      }

      // Los excluidos vienen en un formato separado por comas
      if (item.nota_excluidos) {
        printer.addTextSize(2, 1);
        printer.addText(`${item.nota_excluidos.split(', ').map(ingr => `Sin ${ingr}`).join(', ')} \n`);
      }
      printer.addTextSize(1, 1);
      printer.addText(`${''.padEnd(47, '. ')} \n`);
      printer.addFeedLine(1);
    });

    // Finish Printing
    printer.addCut(printer.CUT_FEED);
  }

  // original
  imprimir(ordenCompleta: Order, impresora) {

    const host = impresora.name;
    const ip = impresora.ip;
    const pedido = ordenCompleta;

    console.log('EPSON DRIVER: Intentando conectar con el Host: ' + host + ' en la IP: ' + ip);
    let printer = null;
    const EPSON = new epson.ePOSDevice();

    EPSON.connect(ip, '8008', (EPSON_state) => {

      console.log('EPSON DRIVER: La impresora ha respondido...');
      console.log([EPSON_state, EPSON]);
      if (EPSON_state === 'OK' || EPSON_state === 'SSL_CONNECT_OK') {
        console.log('EPSON DRIVER: La impresora aceptó la conexion, se crea un Dispositivo de Impresión');
        EPSON.createDevice('local_printer', EPSON.DEVICE_TYPE_PRINTER, { crypto: true, buffer: false }, (DEVICE, CODE) => {
          if (CODE === 'OK') {
            printer = DEVICE;
            console.log('EPSON DRIVER: Dispositivo preparado, se exporta la impresora al Callback');
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            printer.addFeedLine(1);
            printer.addTextAlign(printer.ALIGN_CENTER);
            printer.addTextSize(3, 2);
            printer.addText(pedido.order_code);
            printer.addText(`\n\n`);
            printer.addTextSize(2, 1);
            printer.addText(`Tiempo: ${pedido.tiempo} min \n`);
            printer.addTextSize(1, 1);

            printer.addFeedLine(1);
            printer.addTextAlign(printer.ALIGN_LEFT);
            printer.addText(`Recibido el: ${moment(pedido.created_at).format('dddd, MMMM Do YYYY, H:mm')} \n`);
            printer.addText(`Razón Social: ${pedido.client ? pedido.client.nombre : 'Cliente Particular'} \n`);
            printer.addFeedLine(1);

            if (pedido.nombre_cliente || pedido.nota) {
              printer.addTextAlign(printer.ALIGN_CENTER);
              printer.addText(`${''.padEnd(47, '-')} \n`);
              printer.addFeedLine(1);

              if (pedido.nombre_cliente) {
                printer.addText('Quien recoge el pedido:\n');
                printer.addTextSize(2, 1);
                printer.addText(pedido.nombre_cliente);
                printer.addTextSize(1, 1);
                printer.addText(`\n`);
              }

              if (pedido.nota) {
                printer.addText('Nota del cliente: \n');
                printer.addTextSize(2, 1);
                printer.addText(pedido.nota);
                printer.addTextSize(1, 1);
                printer.addText(`\n`);
              }

              printer.addText(`${''.padEnd(47, '-')} \n`);
              printer.addFeedLine(1);

              printer.addTextAlign(printer.ALIGN_LEFT);
              printer.addFeedLine(1);
            }

            // Impresion de Productos
            pedido.items.forEach(item => {

              // Evitar imprimir items que no son productos
              if (!item.dvproduct_id) { return; }

              printer.addTextSize(3, 2);
              printer.addTextAlign(printer.ALIGN_LEFT);
              printer.addText(`${item.cantidad} x`);

              printer.addTextSize(2, 2);
              printer.addText(`${item.item_name} \n`);

              if (item.nota_variaciones) {
                printer.addTextSize(2, 1);
                printer.addText(`${item.nota_variaciones} \n`);
              }

              // Los excluidos vienen en un formato separado por comas
              if (item.nota_excluidos) {
                printer.addTextSize(2, 1);
                printer.addText(`${item.nota_excluidos.split(', ').map(ingr => `Sin ${ingr}`).join(', ')} \n`);
              }
              printer.addTextSize(1, 1);
              printer.addText(`${''.padEnd(47, '. ')} \n`);
              printer.addFeedLine(1);
            });

            // Finish Printing
            printer.addCut(printer.CUT_FEED);

            // printer.send();
            /////////////////////////////////////////////////////////////////////////////////////////////////
            printer.addFeedLine(1);
            printer.addTextAlign(printer.ALIGN_CENTER);
            printer.addTextSize(3, 2);
            printer.addText(`${pedido.order_code} - ${pedido.pagado ? 'Pagado' : 'No Pagado'} `);
            printer.addTextSize(1, 1);
            printer.addFeedLine(1);

            printer.addFeedLine(1);
            printer.addTextAlign(printer.ALIGN_LEFT);
            printer.addTextSize(1, 1);

            printer.addText(`Recibido el: ${moment(pedido.created_at).format('dddd, MMMM Do YYYY, H:mm')} \n`);
            printer.addText(`Razón Social: ${pedido.client ? pedido.client.nombre : 'Cliente Particular'} \n`);

            if (pedido.zone) {
              printer.addText(`Dirección: ${pedido.zone.nombre} (${pedido.zone.code}) ${pedido.dir}, ${pedido.dir2} \n`);
            }
            else {
              printer.addText('Dirección: Dentro del Local\n');
            }

            printer.addText(`Método de Pago: ${pedido.pay_method.nombre} \n`);
            printer.addText(''.padStart(48, '.'));
            printer.addFeedLine(1);

            pedido.items.forEach(item => {
              printer.addText(item.item_name.length > 30 ? item.item_name.substring(0, 30) : item.item_name.padEnd(30));

              printer.addText('  ');
              printer.addText(item.cantidad ? ('x' + item.cantidad).padStart(6) : ''.padStart(6));

              printer.addText('  ');
              printer.addText((item.subtotal.toLocaleString('es') + ' €').padStart(8));
              printer.addText('\n');
            });

            printer.addText('--------'.padStart(48));
            printer.addText('TOTAL COMPRA');
            printer.addTextSize(2, 1);
            printer.addText((pedido.total.toLocaleString('es') + ' €').padStart(18, '. '));

            printer.addFeedPosition(printer.FEED_PEELING);
            printer.addFeedLine(3);

            // Finish Printing
            printer.addCut(printer.CUT_FEED);
            printer.send();

            EPSON.disconnect();
            this.showAlertOk('Notificación', 'done', 'Exitoooooooooooo');
          } else {
            console.error('Error al Imprimir: ' + CODE);
            this.showAlertOk('Notificación', 'info', 'Error al Imprimir: ' + CODE);
            EPSON.disconnect();
          }
        });
      }
      else {
        console.error('Error al conectar con la Impresora: ' + EPSON_state);
        this.showAlertOk('Notificación', 'info', 'Error al conectar con la Impresora: ' + EPSON_state);
        EPSON.disconnect();
      }
    });
  }

  async showToast(mensaje, icon?, duracion?) {
    const toast = await this.toastController.create({
      duration: duracion ? duracion : 2000,
      message: icon ? `${mensaje} <ion-icon name = '${icon}' > </ion-icon>` : mensaje,
      mode: 'ios',
      position: 'top',
      cssClass: 'toastNotification',
    });
    toast.present();
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
}   //  fin clase ppal

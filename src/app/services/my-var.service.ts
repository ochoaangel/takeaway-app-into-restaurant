import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';

@Injectable({
  providedIn: 'root',
})
export class MyVarService {
  ////////////////////////////////////////////////////////////////////////////
  ///////////////// variables Importantes para compilación ///////////////////

  //  código de la aplicaciòn general (uno para cata tienda)
  uid = 'TR23304797T53H9P8C2HK557G2';
  FirebaseSenderID = '557480860806';
  OneSignalAppID = '8ebf56b6-e1e2-40e8-9cb3-9044ab698efd';


  // //  código de la aplicaciòn general (uno para cata tienda)
  // uid = 'TR23304797T53H9P8C2HK557G2';
  // FirebaseSenderID = '557480860806';
  // OneSignalAppID = '8ebf56b6-e1e2-40e8-9cb3-9044ab698efd';








  DvDeviceUid = '';
  urlBaseImg = 'https://backend.takeaway.cool';
  access_token = null;
  user;
  PrinterSelected;
  dispatch_mode: string = null;
  // updatePedidosNow = false;

  public updatePedidosNow = new Subject<any>();
  initPedidos = null;
  // urlBaseImg = 'https://backend.takeaway.cool';

  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////// TIENDA //////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  ///////////// variables iniciales para arrancar páginas  ///////////////////




  // initRestaurantMenu;
  // initShowProduct;            // idProduct
  // initReadMore;               // idProduct
  // initAddToCart;              // idProduct
  // initPayment;                // {launch:"..",  KO:"..", OK:".."}
  // initMenuCategoryId = -1;    // id de categoria referenciado desde el home
  // cantidadCarrito = 0;              // cantidad de productos en el carrito

  ////////////////////////////////////////////////////////////////////////////
  ///////////// varibles utiles generales



  // appInfo;
  // products;
  // categories;
  // avoidSplash = false;          // usado cuando inicia en false y luego del splash se pone a true(no muestra el splash al retroceder)
  // searchText = '';              // texto a buscar en Home y carta


  ////////////////////////////////////////////////////////////////////////////
  //////////// variable dinamica
  public carritoDinamico = new Subject<any>();

  constructor() { }

  // trabaja en conjunto con variable carrito Dinamico
  carritoActualizar(val) {
    this.carritoDinamico.next(val);
  }

  ////// https://stackoverflow.com/questions/41915625/how-to-detect-variable-change-in-angular2
  /////////////////////////////////////////////////////////// En Servicio
  // import { Subject } from 'rxjs/internal/Subject';
  // public monitorVariableSolicitudes = new Subject<any>();
  // variableSolicitudes(val) {this.monitorVariableSolicitudes.next(val);}
  /////////////////////////////////////////////////////////// en donde se manda
  // this.mys.variableSolicitudes(this.solicitudes);
  /////////////////////////////////////////////////////////// en donde se Recibe
  // this.mys.monitorVariableSolicitudes.subscribe(value => {
  //   this.solicitudes = value;
  // })
}

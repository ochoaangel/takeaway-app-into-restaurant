import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MyVarService } from './my-var.service';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root',
})
export class InterceptorService implements HttpInterceptor {
  constructor(private myV: MyVarService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let myData = {
      // 'TKW-REST-UID': this.myV.uid
    };


    //////////////////////////////////////////////////////////////////////////////////
    // ID de ONESIGNAL
    if (this.myV.DvDeviceUid) {
      myData['DVDEVICE-UID'] = this.myV.DvDeviceUid;
    } else {
      myData['DVDEVICE-UID'] = 'XXXXXXXXX-YYYYYYYY-ZZZZZ';
    }

    // ID Usuario
    // if (this.myV.userUid) {
    //   myData['DVCLIENT-ID'] = this.myV.userUid;
    // } else {
    //   // myData['DVCLIENT-ID'] = 'XXXXXXXXX-YYYYYYYY-ZZZZZ';
    // }

    // ID Usuario
    if (this.myV.access_token) {
      myData['Authorization'] = `Bearer ${this.myV.access_token}`;
    } else {
      console.error('VERIFICAR (excepto LOGIN), no tengo el access_token');
    }

    // console.log('Cabecera del Interceptor', myData);
    const headers = new HttpHeaders(myData);
    const reqClone = req.clone({ headers });
    return next.handle(reqClone).pipe(catchError(this.manejarError));
  }

  // caso de error en todas las consultas
  manejarError(error: HttpErrorResponse) {
    console.warn('Error gestionado en el Servicio Interceptor', error);
    // this.router.navigateByUrl('/no-signal');
    return throwError('Error personalizado');
  }

}

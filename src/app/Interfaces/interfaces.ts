////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////// PROPIAS //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////// EXTERNAS //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//      delivery/orders/1/cancel  dispatch  accept
export interface ApiOrder {
    ok: boolean;
    Despachado: Order[];
    EnPreparacion: Order[];
    Pendiente: Order[];
}


export interface ApiDvOrder {
    ok: boolean;
    order: Order;
}

export interface Order {
    id: number;
    uid: string;
    tkwrest_id: number;
    tkwpaymethod_id: number;
    dvorderbreak_id?: any;
    mgpaydriver_id: number;
    tkwrestzone_id: number;
    dvdevice_uid: string;
    dvclient_id: number;
    total: number;
    pagado: number;
    ref_transaccion?: any;
    tiempo: string;
    razon_anulado?: string;
    order: number;
    order_code: string;
    dir?: string;
    dir2?: string;
    // estado: string;
    estado: 'Pendiente' | 'EnPreparacion' | 'Despachado' | 'Anulado' | 'Recibido';
    nota?: string;
    nombre_cliente?: string;
    aceptado_at?: string;
    enviado_at?: any;
    despachado_at?: string;
    recibido_at?: any;
    anulado_at?: string;
    created_at?: string;
    updated_at: string;

    dispatch_date?: string;
    is_overdue?: boolean;

    client: Client;
    board: Board;

    resumenProductos?: string;


    items?: Item[];
    pay_method?: Paymethod;
    via: 'app' | 'web' | 'kiosoco';
    zone?: Zone;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//      delivery/ordersLive
export interface ApiOrderLive {
    data?: Data;
    status?: string;
    dispatch_mode?: string;
}

export interface Data {
    Pendiente: Order[];
    Despachado: Order[];
    EnPreparacion: Order[];
}

export interface Client {
    email: string;
    nombre: string;
    tel: string;
    id: number;
}

export interface Board {
    group?: string;
    nombre: string;
    id: number;
}


export interface Zone {
    id: number;
    nombre: string;
    code: string;
    ciudad: string;
    recargo: number;
    status: string;
}

export interface Paymethod {
    id: number;
    uid: string;
    nombre: string;
    icon: string;
    status: string;

    driver?: MgPayDriver;
}

export interface MgPayDriver {
    id?: number;
    nombre?: string;
    type?: string;
    driver?: string;
}

export interface Item {
    id: number;
    tkwrest_id: number;
    dvorder_id: number;
    dvproduct_id?: number;
    name_item?: string[];
    cantidad?: number;
    precio?: number;
    precio_vars?: number;
    nota_variaciones?: string;
    nota_excluidos?: string;
    nota_alergenos?: any;
    precio_neto: number;
    subtotal: number;
    created_at: string;
    updated_at: string;
    item_name: string;

    resumenExcluido?: string;

    product?: Product;
}

export interface Product {
    id: number;
    nombre: string;
}




export interface ApiList {
    ok: boolean;
    data: Lista[];
}

export interface Lista {
    id: number;
    nombre: string;
    code: string;
    status: string;
}

export interface ApiTurn {
    ok: boolean;
    data: Turn;
}

export interface ApiTurn1 {
    ok: boolean;
    data: Turn[];
}

export interface ApiTurn2 {
    ok: boolean;
    data: Turn[];
}


export interface Turn {
    id: number;
    tkwrest_id: number;
    tipo: 'L' | 'D';
    dia: 0 | 1 | 2 | 3 | 4 | 5 | 6;
    hora_inicio: string;
    hora_fin: string;
    activo: 0 | 1;
    created_at: string;
    updated_at: string;
    dayName?: 'LUN' | 'MAR' | 'MIE' | 'JUE' | 'VIE' | 'SAB' | 'DOM';

}


export interface ApiSee {
    ok: boolean;
    data: Semana;
}

export interface Semana {
    DOM: Turn[];
    LUN: Turn[];
    MAR: Turn[];
    MIE: Turn[];
    JUE: Turn[];
    VIE: Turn[];
    SAB: Turn[];
}

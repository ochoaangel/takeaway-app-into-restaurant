import { MyVarService } from 'src/app/services/my-var.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { MyStorageService } from '../services/my-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private myV: MyVarService,
    private myS: MyStorageService,
    private router: Router,

  ) { }

  canActivate(): boolean {

    if (this.myV.user) {
      return true;
    } else {


      this.myS.get('user').subscribe(respGetUser => {

        if (respGetUser && respGetUser.email) {
          this.myV.user = respGetUser;
          return true;
        } else {
          return false;
          this.router.navigateByUrl('/login');
        }
      });



    }
  }

}

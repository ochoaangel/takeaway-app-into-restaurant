import { MyVarService } from 'src/app/services/my-var.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  nombreCajero = '';
  nombreEmpresa = '';
  urlLogo = 'assets/images/TakeAway-Logo-2-3.png';

  constructor(
    private myV: MyVarService
  ){}

  ngOnInit() {
    this.nombreCajero = this.myV.user.user.name || 'Usuario de la App';
    this.nombreEmpresa = this.myV.user.rest.nombre || 'Takeaway.cool for Restaurants';
  }

}

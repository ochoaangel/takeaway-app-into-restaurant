import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    // path: 'tabs',
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'inicio',
        loadChildren: () => import('./inicio/inicio/inicio.module').then(m => m.InicioPageModule)
      },
      {
        path: 'reservas',
        loadChildren: () => import('./reservas/reservas/reservas.module').then(m => m.ReservasPageModule)
      },
      {
        path: 'perfil',
        loadChildren: () => import('./perfil/perfil/perfil.module').then(m => m.PerfilPageModule)
      },

      {
        path: 'pedidos',
        children: [
          {
            path: 'pedidos',
            loadChildren: () => import('./pedidos/pedidos.module').then(m => m.PedidosPageModule)
          },
          {
            path: '',
            redirectTo: '/tabs/pedidos/pedidos',
            pathMatch: 'full'
          },
          {
            path: '**',
            redirectTo: '/tabs/pedidos/pedidos',
            pathMatch: 'full'
          },
        ]
      },

      {
        path: 'restaurante',
        children: [
          {
            path: 'restaurante',
            loadChildren: () => import('./restaurante/restaurante.module').then(m => m.RestaurantePageModule)
          },
          {
            path: '',
            redirectTo: '/tabs/restaurante/restaurante',
            pathMatch: 'full'
          },
          {
            path: '**',
            redirectTo: '/tabs/restaurante/restaurante',
            pathMatch: 'full'
          },
        ]
      },

      {
        path: '',
        redirectTo: 'inicio',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/inicio',
    pathMatch: 'full'
  },



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }

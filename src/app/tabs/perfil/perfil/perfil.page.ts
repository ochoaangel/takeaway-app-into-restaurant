import { AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ApiCallService } from 'src/app/services/api-call.service';
import { MyVarService } from 'src/app/services/my-var.service';
import { Router } from '@angular/router';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { MyStorageService } from 'src/app/services/my-storage.service';
declare var epson: any;
// declare var window;


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  loading = 0;
  user = { email: '', password: '' };

  constructor(
    private apiCall: ApiCallService,
    public myV: MyVarService,
    private router: Router,
    private myF: MyFunctionsService,
    private myS: MyStorageService,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.user = { email: '', password: '' };
  }

  btnCerrar() {
    console.log('this.user', this.user);
    this.loading++;
    let myData = {
      email: this.user.email,
      password: this.user.password,
      dvdevice_uid: this.myV.DvDeviceUid
    };
    this.apiCall.logOut(myData).subscribe(resp => {
      this.loading--;

      console.log('resp', resp);
      if (resp.message === 'Successfully logged out') {
        this.myS.remove('user').subscribe(respEliminar => {
          this.myV.access_token = null;
          this.myV.user = null;
          console.log('respEliminar', respEliminar);
          this.myF.showAlertOk('¡Éxito!', 'done', 'Cerrada sesión Exitosamente..');
          this.router.navigateByUrl('/tabs/login');
          this.user = { email: '', password: '' };
        })

      } else {
        this.myF.showAlertOk('Notificación', 'info', 'Datos incorrectos, intente nuevamente..');
      }
    });
  }


}

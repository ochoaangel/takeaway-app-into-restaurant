import { MyPipesModule } from './../../pipes/my-pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RestaurantePageRoutingModule } from './restaurante-routing.module';
import { RestaurantePage } from './restaurante.page';
import { MyComponentsModule } from 'src/app/components/my-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestaurantePageRoutingModule,
    MyComponentsModule,
    MyPipesModule
  ],
  declarations: [RestaurantePage]
})
export class RestaurantePageModule { }

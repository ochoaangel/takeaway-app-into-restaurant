import { filter } from 'rxjs/operators';
import { ApiList, Lista, Turn } from './../../Interfaces/interfaces';
import { Component, OnInit } from '@angular/core';
import { ApiCallService } from 'src/app/services/api-call.service';
import { MyVarService } from 'src/app/services/my-var.service';
import { Router } from '@angular/router';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import * as moment from 'moment';
import * as _ from 'underscore';
import { MyStorageService } from 'src/app/services/my-storage.service';

@Component({
  selector: 'app-restaurante',
  templateUrl: './restaurante.page.html',
  styleUrls: ['./restaurante.page.scss'],
})
export class RestaurantePage implements OnInit {

  loading = 0;
  constructor(
    private apiCall: ApiCallService,
    private myV: MyVarService,
    private router: Router,
    private myF: MyFunctionsService,
    private myS: MyStorageService,
  ) { }

  allHorarios;
  allZonas;
  allPago;
  allProductos;
  allImpresoras;
  allCategorias;

  // panel Principal Restaurante
  showPanelPrincipal = true;

  showHorarios = false;
  showZonas = false;
  showPago = false;
  showProductos = false;
  showImpresoras = false;
  showCategorias = false;
  showHorarioSemanaCocina = false;
  showHorarioSemanaDelivery = false;
  showHorarioNuevoDelivery = false;
  showHorarioTurnoDelivery = false;
  showHorarioNuevoCocina = false;
  showHorarioTurnoCocina = false;
  showImpresoraNueva = false;

  allPrinter = [];

  semana = JSON.parse('{"ok":true,"data":{"Wtf?":[{"id":5,"tkwrest_id":1,"tipo":"L","dia":0,"hora_inicio":"20:00","hora_fin":"23:15","activo":1,"created_at":"2020-07-01 11:19:21","updated_at":"2020-07-01 11:19:21","dayName":"Wtf?"}],"LUN":[{"id":3,"tkwrest_id":1,"tipo":"L","dia":1,"hora_inicio":"20:00","hora_fin":"23:15","activo":1,"created_at":"2020-07-01 11:02:32","updated_at":"2020-07-01 11:02:32","dayName":"LUN"},{"id":4,"tkwrest_id":1,"tipo":"L","dia":1,"hora_inicio":"20:00","hora_fin":"23:15","activo":1,"created_at":"2020-07-01 11:02:40","updated_at":"2020-07-01 11:02:40","dayName":"LUN"},{"id":6,"tkwrest_id":1,"tipo":"L","dia":1,"hora_inicio":"20:00","hora_fin":"23:15","activo":1,"created_at":"2020-07-01 11:19:32","updated_at":"2020-07-01 11:19:32","dayName":"LUN"}],"MAR":[{"id":7,"tkwrest_id":1,"tipo":"L","dia":2,"hora_inicio":"20:00","hora_fin":"23:15","activo":1,"created_at":"2020-07-01 11:19:38","updated_at":"2020-07-01 11:19:38","dayName":"MAR"}],"MIE":[{"id":8,"tkwrest_id":1,"tipo":"L","dia":3,"hora_inicio":"20:00","hora_fin":"23:15","activo":1,"created_at":"2020-07-01 11:19:54","updated_at":"2020-07-01 11:19:54","dayName":"MIE"}],"JUE":[{"id":9,"tkwrest_id":1,"tipo":"L","dia":4,"hora_inicio":"20:00","hora_fin":"23:15","activo":1,"created_at":"2020-07-01 11:19:59","updated_at":"2020-07-01 11:19:59","dayName":"JUE"}],"VIE":[{"id":10,"tkwrest_id":1,"tipo":"L","dia":5,"hora_inicio":"20:00","hora_fin":"23:15","activo":1,"created_at":"2020-07-01 11:20:05","updated_at":"2020-07-01 11:20:05","dayName":"VIE"}],"SAB":[{"id":11,"tkwrest_id":1,"tipo":"L","dia":6,"hora_inicio":"20:00","hora_fin":"23:15","activo":1,"created_at":"2020-07-01 11:20:11","updated_at":"2020-07-01 11:20:11","dayName":"SAB"}]}}');

  turnoDias;
  turnoOneDia;
  turnoNuevo = { tipo: '', inicio: '', fin: '', minFinTime: '' };


  impresoraNueva = { name: '', ip: '', estado: false };
  impresoraSeleccionada = null;

  // show
  ngOnInit() {
    //get impresoras from localstorage and save it to allPrinters
    this.getImpresoras();
  }

  ionViewWillEnter() {
    this.initFromPanel('panelPrincipal');
  }


  ocultarTodo() {
    this.showPanelPrincipal = false;
    this.showHorarios = false;
    this.showZonas = false;
    this.showPago = false;
    this.showProductos = false;
    this.showImpresoras = false;
    this.showCategorias = false;
    this.showHorarioSemanaCocina = false;
    this.showHorarioSemanaDelivery = false;
    this.showHorarioNuevoDelivery = false;
    this.showHorarioTurnoDelivery = false;
    this.showHorarioNuevoCocina = false;
    this.showHorarioTurnoCocina = false;
    this.showImpresoraNueva = false;
  }

  /**
   * 
   * @param nextPanel 'panelPrincipal | horarios | horarioSemana | horarioNuevo | zonas | pago | productos | impresoras '
   */
  initFromPanel(nextPanel, data?) {


    this.ocultarTodo();

    switch (nextPanel) {
      /////////////////////////////////////////////////////////////////////////////////////
      case 'panelPrincipal':
        this.showPanelPrincipal = true;

        break;
      /////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////////

      /////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////////
      case 'horarios':
        this.turnoDias = null;
        this.turnoOneDia = null;
        this.showHorarios = true;
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////////
      case 'horarioSemanaDelivery':
        data ? this.turnoNuevo.tipo = data : null;
        this.getHorarioSemana(this.turnoNuevo.tipo);
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      case 'horarioSemanaCocina':
        data ? this.turnoNuevo.tipo = data : null;
        this.getHorarioSemana(this.turnoNuevo.tipo);
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////////
      case 'horarioTurnoDelivery':
        data ? this.turnoOneDia = data : null;
        this.turnoNuevo.inicio = null;
        this.turnoNuevo.fin = null;
        this.showHorarioTurnoDelivery = true;
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      case 'horarioTurnoCocina':
        data ? this.turnoOneDia = data : null;
        this.turnoNuevo.inicio = null;
        this.turnoNuevo.fin = null;
        this.showHorarioTurnoCocina = true;

        break;
      /////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////////
      case 'horarioNuevoDelivery':
        this.showHorarioNuevoDelivery = true;
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      case 'horarioNuevoCocina':
        this.showHorarioNuevoCocina = true;
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////////

      /////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////////
      case 'zonas':
        this.loading++;
        this.apiCall.controlsList({ list: 'zones' }).subscribe((resp: ApiList) => {
          this.loading--;
          if (resp.data) {
            this.allZonas = resp.data.map(x => {
              x['on'] = x.status === 'A' ? true : false;
              return x;
            });
            console.log('this.all', this.allZonas);

          } else {
            this.myF.showAlertOk('ERROR', 'info', 'Hubo un error al ingresar a esta sección');
          }
        });

        this.showZonas = true;
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      case 'pago':
        this.loading++;
        this.apiCall.controlsList({ list: 'payMethods' }).subscribe((resp: ApiList) => {
          this.loading--;
          if (resp.data) {
            this.allPago = resp.data.map(x => {
              x['on'] = x.status === 'A' ? true : false;
              return x;
            });
            console.log('this.all', this.allPago);

          } else {
            this.myF.showAlertOk('ERROR', 'info', 'Hubo un error al ingresar a esta sección');
          }
        });
        this.showPago = true;
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      case 'productos':
        this.loading++;
        this.apiCall.controlsList({ list: 'products' }).subscribe((resp: ApiList) => {
          this.loading--;
          if (resp.data) {
            this.allProductos = resp.data.map(x => {
              x['on'] = x.status === 'A' ? true : false;
              return x;
            });
            console.log('this.all', this.allProductos);

          } else {
            this.myF.showAlertOk('ERROR', 'info', 'Hubo un error al ingresar a esta sección');
          }
        });
        this.showProductos = true;
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      case 'impresoras':
        this.showImpresoras = true;
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      case 'impresoraNueva':
        this.showImpresoraNueva = true;
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      case 'categorias':
        this.loading++;
        this.apiCall.controlsList({ list: 'categories' }).subscribe((resp: ApiList) => {
          this.loading--;
          if (resp.data) {
            this.allCategorias = resp.data.map(x => {
              x['on'] = x.status === 'A' ? true : false;
              return x;
            });
            console.log('this.all', this.allCategorias);

          } else {
            this.myF.showAlertOk('ERROR', 'info', 'Hubo un error al ingresar a esta sección');
          }
        });
        this.showCategorias = true;
        break;
      /////////////////////////////////////////////////////////////////////////////////////
      default:
        console.error('Vista NO encontrada');
        break;
    }




  }


  changeList(change: string, id: number, on: boolean) {
    // cuando recibo on true es xq el troggle se se pasó a inactivo
    let next = on ? 'I' : 'A';
    let end = { change, id, next };
    this.loading++;
    this.apiCall.controlsSwitch(end).subscribe(resp => {
      this.loading--;
    });

  }

  /**
   * 
   * @param data tipo Objeto de data ej: {'LUN':collections, 'MAR':collections} 
   */
  convertirRespuestaApiTurnosPorDia(data) {
    let final;

    // convierto objeto en colección con nuevas keys 
    let notAll = _.pairs(data).map(x => {
      let dia;
      let diaN;
      let dia3Letras = x[0];
      let turnos = x[1];

      switch (x[0]) {

        case 'LUN':
          dia = 'Lunes';
          diaN = 1;
          break;

        case 'MAR':
          dia = 'Martes';
          diaN = 2;
          break;

        case 'MIE':
          dia = 'Miércoles';
          diaN = 3;
          break;

        case 'JUE':
          dia = 'Jueves';
          diaN = 4;
          break;

        case 'VIE':
          dia = 'Viernes';
          diaN = 5;
          break;

        case 'SAB':
          dia = 'Sábado';
          diaN = 6;
          break;

        case 'DOM':
          dia = 'Domingo';
          diaN = 7;
          break;

        default:
          dia = 'DIA_NO_RECONOCIDO';
          diaN = 20;
          break;
      }

      let end = {
        dia,
        dia3Letras,
        diaN,
        turnos
      };

      return end;
    });

    // defino parametros inicialescon todos los dias vacios
    let diasValidos = [
      { dia3Letras: 'LUN', diaN: 1, turnos: null, dia: 'Lunes' },
      { dia3Letras: 'MAR', diaN: 2, turnos: null, dia: 'Martes' },
      { dia3Letras: 'MIE', diaN: 3, turnos: null, dia: 'Miércoles' },
      { dia3Letras: 'JUE', diaN: 4, turnos: null, dia: 'Jueves' },
      { dia3Letras: 'VIE', diaN: 5, turnos: null, dia: 'Viernes' },
      { dia3Letras: 'SAB', diaN: 6, turnos: null, dia: 'Sábado' },
      { dia3Letras: 'DOM', diaN: 7, turnos: null, dia: 'Domingo' },
    ];

    diasValidos.forEach(element => {
      let encontrado = null;
      encontrado = notAll.filter(y => y.diaN === element.diaN)[0];

      // solo reemplaza cuando consigue info en turnos
      if (encontrado && encontrado['turnos'].length > 0) {
        element['turnos'] = encontrado['turnos'];
      }
    });

    console.log('diasValidosX', diasValidos);

    diasValidos.forEach(diaSemana => {
      if (diaSemana.turnos) {
        diaSemana.turnos.forEach(diaUno => {
          if (diaUno.activo === 1) {
            diaUno['status'] = true;
          } else {
            diaUno['status'] = false;
          }
        });
      }
    });


    return diasValidos;
  }

  agregarNuevoTurno() {

    if (!this.turnoNuevo.inicio) {
      this.myF.showAlertOk('Notificación', 'info', 'Ingrese Hora de Inicio, intente nuevamente..');
    } else if (!this.turnoNuevo.fin) {
      this.myF.showAlertOk('Notificación', 'info', 'Ingrese Hora de fin, intente nuevamente..');
    } else if (moment(this.turnoNuevo.fin).format('HH:mm') === moment(this.turnoNuevo.inicio).format('HH:mm')) {
      this.myF.showAlertOk('Notificación', 'info', 'Los turnos no deben inicializar y finalizar en el mismo momento, intente nuevamente..');
    } else {
      console.log('this.turnoNuevo', this.turnoNuevo);

      let end = {
        dia: this.turnoOneDia.diaN,
        tipo: this.turnoNuevo.tipo,
        hora_inicio: moment(this.turnoNuevo.inicio).format('HH:mm'),
        hora_fin: moment(this.turnoNuevo.fin).format('HH:mm')
      };

      this.loading++;
      this.apiCall.turnsAdd(end).subscribe(resp => {
        this.loading--;
        console.log('resp', resp);

        if (resp.ok && resp.data) {

          if (!this.turnoOneDia.turnos) {
            this.turnoOneDia.turnos = [];
          }

          // setear turno a activo por default
          resp.data['activo'] = 1;
          resp.data['status'] = true;

          this.turnoOneDia.turnos.push(resp.data);

          console.log('this.turnoOneDia', this.turnoOneDia);

          this.myF.showAlertOk('Éxito', 'done', 'Turno Agregado Exitosamente..');

          if (resp.data.tipo === 'D') {
            this.initFromPanel('horarioTurnoDelivery');
          } else {
            this.initFromPanel('horarioTurnoCocina');
          }

        } else {
          this.myF.showAlertOk('Notificación', 'info', 'No se pudo agregar Turno, intente nuevamente..');
        }
      });

    }



  }

  eliminarTurnoPorId(dvrestturn_id, i) {
    this.loading++;
    console.log('i a eliminar', i);
    console.log('antes', this.turnoOneDia.turnos);
    this.apiCall.turnsRem({ dvrestturn_id }).subscribe(resp => {
      this.loading--;
      if (this.turnoOneDia.turnos.length > 1) {
        this.turnoOneDia.turnos.splice(i, 1);
      } else {
        this.turnoOneDia.turnos = null;
      }

      this.myF.showAlertOk('Notificación', 'done', 'Eliminado Turno Exitosamente..');

      console.log('Despues', this.turnoOneDia.turnos);

      console.log('resp al eliminar', resp);
    });
  }


  getHorarioSemana(DoL) {

    this.loading++;
    this.apiCall.turnsSee({ tipo: DoL }).subscribe(resp => {
      this.loading--;

      console.log('resp de See:', resp);
      if (resp.ok && resp.data) {
        this.turnoDias = this.convertirRespuestaApiTurnosPorDia(resp.data);
        console.log('this.turnoDias', this.turnoDias);
        switch (DoL) {
          case 'D':
            this.showHorarioSemanaDelivery = true;
            break;
          case 'L':
            this.showHorarioSemanaCocina = true;
            break;
          default:
            break;
        }
      } else {
        console.log('ERROR al obtener dias de semana');
        this.myF.showAlertOk('Notificación', 'info', 'No se pudo Obtener los horarios de la semana, intente nuevamente..');
      }

    });




  }

  cambiarStatusTurno(turno, i) {
    console.log('turno', turno);
    let next = '';

    if (turno.status) {
      next = '0';
      this.turnoOneDia.turnos[i]['activo'] = 0;
    } else {
      next = '1';
      this.turnoOneDia.turnos[i]['activo'] = 1;
    }

    let data = { dvrestturn_id: turno.id, next };

    console.log('data', data);

    this.loading++;
    this.apiCall.turnsSwitch(data).subscribe(resp => {
      this.loading--;

      console.log('resp', resp);

    });
  }


  turnoNuevoTimeChange() {
    this.turnoNuevo.minFinTime = moment(this.turnoNuevo.inicio).format('HH:mm');
  }



  ///////////////////////////////////////////////////////////////
  ////////////////////////  IMPRESORA //////////////////////////
  //////////////////////////////////////////////////////////////

  agregarImpresora() {
    if (!(this.allPrinter && this.allPrinter.length >= 0)) {
      this.allPrinter = [];
    }

    this.allPrinter.push({ name: this.impresoraNueva.name, ip: this.impresoraNueva.ip, estado: false });
    console.log('this.allPrinter', this.allPrinter);
    this.setImpresora(this.allPrinter);
    this.myF.showAlertOk('Éxito', 'done', `Agregada <br> nueva impresora ${this.impresoraNueva.name} <br> Exitosamente..`);
    this.impresoraNueva.ip = '';
    this.impresoraNueva.name = '';
    this.initFromPanel('impresoras');


  }

  cambioDeImpresora(n) {
    setTimeout(() => {
      let init = this.allPrinter[n].estado;
      this.allPrinter.forEach(element => {
        element.estado = false;
      });
      this.allPrinter[n].estado = init;
      this.setImpresora(this.allPrinter);
    }, 50);
  }

  eliminarImpresora(n) {
    this.allPrinter.splice(n, 1);
    this.setImpresora(this.allPrinter);
  }

  setImpresora(variable) {

    this.myS.set('impresoras', variable).subscribe(resp2 => {
      let one = variable.filter(resp => resp.estado)[0];
      this.impresoraSeleccionada = one ? one : null;
      console.log('Guardado al Storage.', variable);
      console.log('impresoraSeleccionadaDsdSet', this.impresoraSeleccionada);
    });
  }

  getImpresoras() {
    this.myS.get('impresoras').subscribe(x => {
      this.allPrinter = x;

      if (this.allPrinter) {
        let one = this.allPrinter.filter(resp => resp.estado)[0];
        this.impresoraSeleccionada = one ? one : null;
      } else {
        this.impresoraSeleccionada = null;
      }


      console.log('x desde Get Storage', x);
      console.log('impresoraSeleccionadaDsdGet', this.impresoraSeleccionada);
    });

  }


  probarImpresora(impresora) {
    console.log('impresora', impresora);
    this.myF.imprimirPrueba(impresora).subscribe(imp => {
      if (!imp.status) {
        this.myF.showAlertOk('Notificación', 'info', imp.message);
      }
    }, error => {
      this.myF.showAlertOk('Notificación', 'info', 'Error en la impresión..');
    })
  }

  ///////////////////////////////////////////////////////////////
  //////////////////////////  OTROS ////////////////////////////
  //////////////////////////////////////////////////////////////

}


import { Component, OnInit } from '@angular/core';
import { MyVarService } from 'src/app/services/my-var.service';
import { Router } from '@angular/router';
import { ApiCallService } from 'src/app/services/api-call.service';
import * as moment from 'moment';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  apiHome;
  initTransit;
  loading = 0;

  valor = false;

  constructor(
    private myV: MyVarService,
    private router: Router,
    private apicall: ApiCallService,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loading++;
    this.apicall.home().subscribe(resp => {
      this.loading--;
      console.log('resp', resp);
      this.apiHome = resp.data;
      console.log('this.apiHome', this.apiHome);
    });

    this.loading++;
    this.apicall.initTransit().subscribe(resp => {
      this.loading--;
      this.initTransit = resp.data.slice(0, 5);
      console.log('this.initTransit', this.initTransit);
    });
  }

  btnPedidosHistorico() {
    this.myV.initPedidos = 'historicoPedidos';
    this.router.navigateByUrl('/tabs/pedidos/pedidos');
  }

  btnPedidosTableroReciente() {
    this.myV.initPedidos = 'tableroPedidos';
    this.router.navigateByUrl('/tabs/pedidos/pedidos');
  }

  ionViewWillLeave() {
    this.apiHome = null;
  }

  getdateEntrega(dispatch_date) {
    let end = moment(dispatch_date, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
    return end;
  }

  getdateEntregaDays(item) {

    let { dispatch_date, is_overdue } = item;

    // definiendo las referencias sin horas ni minutos ni segundos
    let now = moment.utc().format('YYYY-MM-DD');
    let dispatch = moment.utc(dispatch_date, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');

    // definiendo los momentos sin horas ni minutos ni segundos
    let now2 = moment(now, 'YYYY-MM-DD');
    let dispatch2 = moment(dispatch, 'YYYY-MM-DD');

    // calculando la cantidad de dias entre las fechas
    let dias = dispatch2.diff(now2, 'days');
    // let dias = now2.diff(dispatch2, 'days');

    let end = '';
    if (is_overdue) {
      end = `ATRASADO`;
    } else if (dias < 0 && is_overdue === null) {
      end = `ATRASADO`;
    } else if (dias === 0) {
      end = `Hoy`;
    } else if (dias === 1) {
      end = `Mañana`;
    } else {
      end = `Dentro de ${dias} días`;
    }

    return end;
  }


  gotoPedidosEnPreparacion() {
    this.myV.initPedidos = 'enPreparacionPedidos';
    this.router.navigateByUrl('/tabs/pedidos')
  }
}

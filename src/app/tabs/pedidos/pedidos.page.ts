import { filter } from 'rxjs/operators';
import { Order, Item, ApiOrder, ApiDvOrder } from './../../Interfaces/interfaces';
import { ApiCallService } from './../../services/api-call.service';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'underscore';
import { MyVarService } from 'src/app/services/my-var.service';
import { Router } from '@angular/router';
import { MyFunctionsService } from 'src/app/services/my-functions.service';
import { ApiOrderLive } from 'src/app/Interfaces/interfaces';
import { AlertController } from '@ionic/angular';
import { MyStorageService } from 'src/app/services/my-storage.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})

export class PedidosPage implements OnInit {

  moment: any = moment;

  // divs Grandes
  showPedidos = false;
  showHistorico = false;
  showHistoricoDetalle = false;
  showHistoricoResultado = false;

  // barra TABS pedidos
  showDespachados = false;
  showRecientes = false;
  showPreparacion = false;

  showOrdenDetalle = false;

  dispatchDate = { fecha: '', minFecha: '', maxFecha: '' };
  rescheduleDispatchDate = { fecha: '', minFecha: '', maxFecha: '' };

  zonas;                        // para buscar en historico de pedidos
  metodosDePago;                // para buscar en historico de pedidos

  estadoPedidos = [
    { nombre: 'Anulado', selected: false },
    { nombre: 'Recibido', selected: true },
    { nombre: 'Despachado', selected: true },
    { nombre: 'EnPreparacion', selected: false },
    { nombre: 'Pendiente', selected: false }
  ];

  myHistoricoPedido = {
    fechaI: '',
    fechaImin: '',
    fechaImax: '',
    horaI: '',
    fechaF: '',
    fechaFmin: '',
    fechaFmax: '',
    horaF: '',
  };

  allHistorico;

  pedidoHistoricoSeleccionado;


  loading = 0;
  nowTime;

  togglePendienteNotificarAlCliente = true;
  togglePendienteImprimePedido = true;

  loopTimeUpdate = 5000;

  animarBtnActualizacion = false;

  lastUpdateTime: string;

  ordenesPendientes: Order[];
  ordenesDespachadas: Order[];
  ordenesEnPreparacion: Order[];

  item: Order;

  printerSelected;

  tiempoDePreparacion = 25;




  constructor(
    private apiCall: ApiCallService,
    public myV: MyVarService,
    private router: Router,
    private myF: MyFunctionsService,
    private myS: MyStorageService,
    private alertCtrl: AlertController
  ) {
  }

  ngOnInit() {

    this.myV.updatePedidosNow.subscribe(resp => {
      this.getPedidosAll();
      this.nowTime = moment().format('YYYY-MM-DD HH:mm:ss');
    });

    // actualizo cada Segundo
    setInterval(() => {
      this.nowTime = moment().format('YYYY-MM-DD HH:mm:ss');
      console.log('Actualizado tiempo de espera automaticamente cada: ', this.loopTimeUpdate + ' ms');
    }, this.loopTimeUpdate);


    // llenando las colecciones para Zonas y metodos de pagos
    this.loading++;
    this.apiCall.controlsList({ list: 'zones' }).subscribe(resp => {
      this.loading--;
      if (resp.ok) {
        this.zonas = resp.data;
        this.zonas.forEach(element => {
          element.selected = false;
        });
        console.log('-----zones', resp.data);
      } else { console.warn('Hubo problemas al obtener Zonas de delivery'); }
    });

    // llenando las colecciones para metodos de pagos
    this.loading++;
    this.apiCall.controlsList({ list: 'payMethods' }).subscribe(resp => {
      this.loading--;
      if (resp.ok) {
        this.metodosDePago = resp.data;
        this.metodosDePago.forEach(element => {
          element.selected = false;
        });
        console.log('-----payMethods', resp.data);
      } else { console.warn('Hubo problemas al obtener Métodos de pago'); }
    });

  }

  ionViewWillEnter() {

    this.dispatchDate.minFecha = moment().format();
    this.dispatchDate.maxFecha = moment().add(10, 'years').format();
    this.rescheduleDispatchDate.minFecha = moment().format();
    this.rescheduleDispatchDate.maxFecha = moment().add(10, 'years').format();

    this.myHistoricoPedido.fechaImax = moment().format();
    this.myHistoricoPedido.fechaImin = moment().subtract(10, 'years').format();
    console.log('this.myHistoricoPedido', this.myHistoricoPedido);



    switch (this.myV.initPedidos) {

      case 'historicoPedidos':
        this.ocultarTodo();
        this.showHistorico = true;
        break;

      case 'enPreparacionPedidos':
        this.ocultarTodo();
        this.showPedidos = true;
        this.showPreparacion = true;
        break;

      default:
        this.ocultarTodo();
        this.showPedidos = true;
        this.showRecientes = true;
        break;
    }


    if (this.myV.access_token) {
      console.log('Tengo Token, Solicitando Pedidos');
      this.getPedidosAll();
    } else {
      console.log('No logueado, iniciando Sesión y solicitando Pedidos');
      this.iniciarSesion();
    }

    // Seleccionar impresora
    this.myS.get('impresoras').subscribe(Impresoras => {
      console.log('Impresoras', Impresoras);
      if (Impresoras && Impresoras.length > 0) {
        const activa = Impresoras.filter(x => x.estado)[0];
        if (activa) {
          this.printerSelected = activa;
          this.togglePendienteImprimePedido = true;
        }
      } else {
        this.printerSelected = null;
        this.togglePendienteImprimePedido = false;
      }
      console.log('this.printerSelected', this.printerSelected);
    });
  }


  ionViewDidLeave() {
    this.clearDataHistorico()
  }


  //////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////// BOTONES TABLERO /////////////////////////////////////////
  btnTabRecientes() {
    this.ocultarListaPedidos();
    this.showRecientes = true;
  }

  btnTabPreparacion() {
    this.ocultarListaPedidos();
    this.showPreparacion = true;
  }

  btnTabDespachados() {
    this.ocultarListaPedidos();
    this.showDespachados = true;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  // botones Varios
  btnActualizarTableroDePedidos() {
    if (this.myV.access_token) {
      console.log('Tengo Token, Solicitando Pedidos');
      this.getPedidosAll();
    } else {
      console.log('No logueado, iniciando Sesión y solicitando Pedidos');
      this.iniciarSesion();
    }
  }

  btnGotoItem(item: Order) {
    this.item = item;
    this.showOrdenDetalle = true;
    console.log('this.item', this.item);
  }

  btnTiempoPreparacionMas() { this.tiempoDePreparacion = this.tiempoDePreparacion + 5; }

  btnTiempoPreparacionMenos() {
    if (this.tiempoDePreparacion > 5) {
      this.tiempoDePreparacion = this.tiempoDePreparacion - 5;
    }
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////// Funciones Varias /////////////////////////////////////////

  ocultarTodo() {
    this.ocultarListaPedidos();
    this.showHistorico = false;
    this.showPedidos = false;
    this.showOrdenDetalle = false;
    this.showHistoricoDetalle = false;
    this.showHistoricoResultado = false;
  }


  ocultarListaPedidos() {
    // paneles principales
    this.showDespachados = false;
    this.showRecientes = false;
    this.showPreparacion = false;
  }

  getPedidosAll() {
    // caso logueado

    this.lastUpdateTime = moment().format('YYYY-MM-DD HH:mm:ss');

    this.loading++;
    this.apiCall.ordersLive().subscribe((resp: ApiOrderLive) => {
      this.loading--;
      console.log('resp', resp);

      if (resp.dispatch_mode) {
        console.log('dispatch_mode:', resp.dispatch_mode);
        this.myV.dispatch_mode = resp.dispatch_mode;
      }



      if (resp.data) {
        // si tngo data guardo las demas Variables
        this.ordenesDespachadas = resp.data.Despachado ? resp.data.Despachado : null;
        this.ordenesEnPreparacion = resp.data.EnPreparacion ? resp.data.EnPreparacion : null;
        this.ordenesPendientes = resp.data.Pendiente ? resp.data.Pendiente : null;

        this.ordenesDespachadas = this.agregarResumenProductosYexcluidos(this.ordenesDespachadas);
        this.ordenesEnPreparacion = this.agregarResumenProductosYexcluidos(this.ordenesEnPreparacion);
        this.ordenesPendientes = this.agregarResumenProductosYexcluidos(this.ordenesPendientes);

      } else {
        this.myF.showAlertOk('Error', 'info', resp.status ? resp.status : 'Error al Obtener Pedidos desde El Servidor..');
      }
      console.log('this.ordenesPendientes', this.ordenesPendientes);
      console.log('this.ordenesDespachadas', this.ordenesDespachadas);
      console.log('this.ordenesEnPreparación', this.ordenesEnPreparacion);
      console.log('this.lastUpdateTime', this.lastUpdateTime);

    });
  }

  iniciarSesion() {
    this.loading++;
    this.apiCall.logIn({ email: 'tester@takeaway.cool', password: 'secret' }).subscribe(resp => {
      this.loading--;
      if (resp && resp.access_token) {
        this.myV.access_token = resp.access_token;
        console.log('LOGEADO con acces_token en this.myV.access_token: ', this.myV.access_token);
        this.getPedidosAll();
      } else {
        this.myF.showAlertOk('Notificación', 'info', 'Datos incorrectos, intente nuevamente..');
      }
    });
  }


  agregarResumenProductosYexcluidos(pedidos: Order[]) {
    if (pedidos && pedidos.length > 0) {
      pedidos.forEach((order: Order) => {
        let texto = '';

        order.items.forEach((product: Item) => {

          if (product.dvproduct_id) {
            // para resumenProducto en la orden
            if (product.cantidad > 1) {
              texto = `${texto}${product.cantidad} ${product.item_name}, `;
            } else {
              texto = `${texto}${product.item_name}, `;
            }
          }

          // para resumenExcluido en cada item de la orden
          if (product.nota_excluidos) {
            let txtExcluyente = product.nota_excluidos;
            txtExcluyente = 'Sin ' + txtExcluyente.replace(/\,/g, ', Sin') + '.';
            product.resumenExcluido = txtExcluyente || null;
          }

        });
        order.resumenProductos = texto.substring(0, texto.length - 2) + '.';
      });



    }

    return pedidos;
  }

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////  API CALL /////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////


  ordenAceptar() {

    let myData = { notify_client: this.togglePendienteImprimePedido };

    if (this.myV.dispatch_mode === 'time' || moment(moment(this.dispatchDate.fecha)).isValid()) {

      // agregando parametro de acuerdo al caso
      this.myV.dispatch_mode === 'time' ?
        myData['tiempo'] = this.tiempoDePreparacion :
        myData['dispatch_date'] = moment(this.dispatchDate.fecha).format('YYYY-MM-DD HH:mm');

      console.log('Datos enviados al Aceptar Orden: ', myData);

      this.apiCall.orderAccept(this.item.id, myData)
        .subscribe((resp: ApiDvOrder) => {
          console.log('resppuestaAlAceptarOrden: ', resp);
          if (resp.ok) {
            this.myF.showAlertOk('Notificación', 'done', `Orden ${resp.order.order_code} Aceptada Exitosamente..`);
            this.showOrdenDetalle = false;

            this.btnTabPreparacion();
            this.btnActualizarTableroDePedidos();

            if (this.togglePendienteImprimePedido) {
              this.item.tiempo = this.tiempoDePreparacion.toString();
              this.item.dispatch_date = this.dispatchDate.fecha;
              this.imprimirAdminAndCocina(this.item, this.printerSelected, true);
            }

            this.tiempoDePreparacion = 25;
            this.dispatchDate.fecha = '';

          } else {
            this.tiempoDePreparacion = 25;
            this.dispatchDate.fecha = '';
            this.myF.showAlertOk('ERROR', 'info', 'La Orden no se pudo enviar a Preparación, intente nuevamente..');
          }
        });
    } else {
      this.myF.showAlertOk('ERROR', 'info', 'Debe ingresar una Fecha de entrega Válida, </br>intente nuevamente..');
    }

  }

  ordenDespachar() {
    this.apiCall.orderDispatch(this.item.id)
      .subscribe((resp: ApiDvOrder) => {
        // console.log('resp', resp);
        if (resp.ok) {
          this.myF.showAlertOk('Notificación', 'done', `Orden ${resp.order.order_code} Enviada a panel de Despachado Exitosamente..`);
          this.showOrdenDetalle = false;
          this.btnTabDespachados();
          this.btnActualizarTableroDePedidos();

        } else {
          this.myF.showAlertOk('ERROR', 'info', 'La Orden no se pudo enviar a Despachado, intente nuevamente..');
        }
      });
  }

  ordenFinalizar() {
    this.apiCall.orderFinish(this.item.id).subscribe((resp: ApiDvOrder) => {
      console.log('resp', resp);
      if (resp.ok) {
        this.myF.showAlertOk('¡¡Felicitaciones!!', 'done', `Orden ${resp.order.order_code} Finalizada Exitosamente..`);
        this.showOrdenDetalle = false;
        this.btnTabRecientes();
        this.btnActualizarTableroDePedidos();
      } else {
        this.myF.showAlertOk('ERROR', 'info', 'La Orden no se pudo Finalizar, intente nuevamente..');
      }
    });
  }

  ordenCancelar(myData) {
    this.apiCall.orderCancel(this.item.id, myData).subscribe((resp: ApiDvOrder) => {
      // console.log('resp', resp);
      if (resp.ok) {
        this.myF.showAlertOk('Notificación', 'done', `Orden ${resp.order.order_code} Anulada Exitosamente..`);
        this.showOrdenDetalle = false;
        this.btnTabRecientes();
        this.btnActualizarTableroDePedidos();
      } else {
        this.myF.showAlertOk('ERROR', 'info', 'La Orden no se pudo anular, intente nuevamente..');
      }
    });
  }

  ////////////////////////////////////////////////////////////////////////////////////

  imprimirAdministrativo(orden, impresora) {
    console.log('impresora', impresora);
    this.myF.imprimirAdmin(orden, impresora).subscribe(imp => {
      if (!imp.status) {
        this.myF.showAlertOk('Notificación', 'info', imp.message);
      }
    }, error => {
      this.myF.showAlertOk('Notificación', 'info', 'Error en la impresión..');
    });
  }

  imprimirCocina(orden, impresora) {
    console.log('impresora', impresora);
    this.myF.imprimirCocina(orden, impresora).subscribe(imp => {
      if (!imp.status) {
        this.myF.showAlertOk('Notificación', 'info', imp.message);
      }
    }, error => {
      this.myF.showAlertOk('Notificación', 'info', 'Error en la impresión..');
    });
  }

  imprimirAdminAndCocina(orden, impresora, onceMoreTicketAdmin = false) {
    console.log('impresora', impresora);
    this.myF.imprimirAdminAndCocina(orden, impresora, onceMoreTicketAdmin).subscribe(imp => {
      if (!imp.status) {
        this.myF.showAlertOk('Notificación', 'info', imp.message);
      }
    }, error => {
      this.myF.showAlertOk('Notificación', 'info', 'Error en la impresión..');
    });
  }

  //////////////////////////////////////////////////////////////////////////////////

  async presentAlertPrompt() {
    const alert = await this.alertCtrl.create({
      cssClass: 'alertTextarea',
      header: 'Anular Orden',
      mode: 'ios',
      inputs: [
        {
          name: 'txtAnular',
          id: 'txtAnular',
          type: 'textarea',
          placeholder: 'Indique el motivo'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Anular Pedido',
          handler: (x) => {
            this.ordenCancelar({ razon_anulado: x.txtAnular || '' });
          }
        }
      ]
    });

    await alert.present();
  }


  irAHistoricoDetalle() {
    console.log('------');
  }

  cambioFechaBuscarHistoricoPedido() {
    let fcalculo = moment(this.myHistoricoPedido.fechaI).add(4, 'months').format();
    let actual = moment().format();
    let valida = moment(fcalculo).isAfter(moment()) ? moment().format() : moment(fcalculo).format();
    this.myHistoricoPedido.fechaFmax = valida;
    this.myHistoricoPedido.fechaFmin = this.myHistoricoPedido.fechaI;
    this.myHistoricoPedido.fechaF = null;
  }

  pruebaBorrar() {
    console.log('this.zonas', this.zonas);
    console.log('this.metodosDePago', this.metodosDePago);
  }

  btnHistoricoPedidos() {
    this.ocultarTodo();
    this.showHistorico = true;
  }

  btnBuscarHistoricoPedidos() {
    let data = { format: 'all' };

    if (!this.myHistoricoPedido.fechaF && this.myHistoricoPedido.fechaI) {
      this.myHistoricoPedido.fechaF = this.myHistoricoPedido.fechaI;
    }


    // procesando fechas
    if (this.myHistoricoPedido.fechaI) {
      data['ini_date'] = moment(this.myHistoricoPedido.fechaI).format('YYYY-MM-DD');
    }
    if (this.myHistoricoPedido.fechaF) {
      data['fin_date'] = moment(this.myHistoricoPedido.fechaF).format('YYYY-MM-DD');
    }

    // procesando horas
    if (this.myHistoricoPedido.horaI) {
      data['ini_time'] = moment(this.myHistoricoPedido.horaI).format('HH:mm');
    } else { data['ini_time'] = '00:00'; }
    if (this.myHistoricoPedido.horaF) {
      data['fin_time'] = moment(this.myHistoricoPedido.horaF).format('HH:mm');
    } else { data['fin_time'] = '23:59'; }

    // procesando los estados seleccionados
    const estado: any = this.estadoPedidos.filter(x => x.selected);
    if (estado.length > 0) {
      if (estado.length === 1) {
        data['estado'] = estado[0].nombre;
      } else {
        data['estado'] = estado.map(x => x.nombre);
      }
    }

    // procesando las zonas seleccionados
    const zone = this.zonas.filter(x => x.selected);
    if (zone.length > 0) {
      if (zone.length === 1) {
        data['zone'] = zone[0].id;
      } else {
        data['zone'] = zone.map(x => x.id);
      }
    }

    // procesando los payMethod seleccionados
    const payMethod = this.metodosDePago.filter(x => x.selected);
    if (payMethod.length > 0) {
      if (payMethod.length === 1) {
        data['payMethod'] = payMethod[0].id;
      } else {
        data['payMethod'] = payMethod.map(x => x.id);
      }
    }



    console.log('this.zonas', this.zonas);
    console.log('this.payMethod', this.metodosDePago);
    console.log('data enviada para buscar pedidos', data);

    this.loading++;
    this.apiCall.buscarHistoricoPedido(data).subscribe(allHist => {
      this.loading--;
      console.log('allHist', allHist);
      if (allHist.count > 0) {
        this.allHistorico = allHist;
        this.ocultarTodo();
        this.showHistoricoResultado = true;
      } else {
        this.myF.showAlertOk('Notificación', 'info', 'No se obtuvo resultados, intente nuevamente con otros parámetros..');
      }
    });
  }



  seleccionandoPedidoHistorico(pedido) {
    this.loading++;
    this.apiCall.verDetalleHistoriPedido({ id: pedido.id }).subscribe(resp => {
      this.loading--;
      this.pedidoHistoricoSeleccionado = pedido;
      this.pedidoHistoricoSeleccionado['view'] = resp.data;
      console.log('pedidoHistoricoSeleccionado', this.pedidoHistoricoSeleccionado);
    })




    this.ocultarTodo();
    this.showHistoricoDetalle = true;
  }


  clearDataHistorico() {
    this.myHistoricoPedido.fechaF = '';
    this.myHistoricoPedido.fechaFmax = '';
    this.myHistoricoPedido.fechaFmin = '';
    this.myHistoricoPedido.fechaI = '';
    this.myHistoricoPedido.fechaImax = '';
    this.myHistoricoPedido.fechaImin = '';
    this.myHistoricoPedido.horaF = '';
    this.myHistoricoPedido.horaI = '';
    this.allHistorico = null;
    this.pedidoHistoricoSeleccionado = null;
  }


  ordenReagendarEnCasoDate() {
    let myData = {};
    myData['dispatch_date'] = moment(this.rescheduleDispatchDate.fecha).format('YYYY-MM-DD HH:mm');
    this.apiCall.orderPrepareOnlyInDispashdateDate(this.item.id, myData)
      .subscribe((resp: ApiDvOrder) => {
        if (resp.ok) {
          this.myF.showAlertOk('Notificación', 'done', `Orden ${resp.order.order_code} Reagendada Exitosamente..`);
          this.showOrdenDetalle = false;

          this.btnTabRecientes();
          this.btnActualizarTableroDePedidos();

          if (this.togglePendienteImprimePedido) {
            this.item.tiempo = this.tiempoDePreparacion.toString();
            this.item.dispatch_date = this.rescheduleDispatchDate.fecha;
            this.imprimirAdminAndCocina(this.item, this.printerSelected, true);
          }

          this.tiempoDePreparacion = 25;
          this.rescheduleDispatchDate.fecha = '';

        } else {
          this.tiempoDePreparacion = 25;
          this.rescheduleDispatchDate.fecha = '';
          this.myF.showAlertOk('ERROR', 'info', 'La Orden no se pudo Reagendar, intente nuevamente..');
        }
      });
  }

}

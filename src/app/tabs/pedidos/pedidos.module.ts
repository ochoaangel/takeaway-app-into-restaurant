import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PedidosPageRoutingModule } from './pedidos-routing.module';
import { PedidosPage } from './pedidos.page';
import { MyComponentsModule } from 'src/app/components/my-components.module';
import { MyPipesModule } from 'src/app/pipes/my-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedidosPageRoutingModule,
    MyComponentsModule,
    MyPipesModule
  ],
  declarations: [PedidosPage]
})
export class PedidosPageModule { } 

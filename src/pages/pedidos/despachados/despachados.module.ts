import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DespachadosPageRoutingModule } from './despachados-routing.module';
import { DespachadosPage } from './despachados.page';
import { MyComponentsModule } from 'src/app/components/my-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DespachadosPageRoutingModule,
    MyComponentsModule
  ],
  declarations: [DespachadosPage]
})
export class DespachadosPageModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RecientesPage } from './recientes.page';

describe('RecientesPage', () => {
  let component: RecientesPage;
  let fixture: ComponentFixture<RecientesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecientesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RecientesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

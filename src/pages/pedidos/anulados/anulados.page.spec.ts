import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AnuladosPage } from './anulados.page';

describe('AnuladosPage', () => {
  let component: AnuladosPage;
  let fixture: ComponentFixture<AnuladosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnuladosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AnuladosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

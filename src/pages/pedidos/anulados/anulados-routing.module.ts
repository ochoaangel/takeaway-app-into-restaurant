import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnuladosPage } from './anulados.page';

const routes: Routes = [
  {
    path: '',
    component: AnuladosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnuladosPageRoutingModule {}

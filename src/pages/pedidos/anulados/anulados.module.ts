import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AnuladosPageRoutingModule } from './anulados-routing.module';
import { AnuladosPage } from './anulados.page';
import { MyComponentsModule } from 'src/app/components/my-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AnuladosPageRoutingModule,
    MyComponentsModule
  ],
  declarations: [AnuladosPage]
})
export class AnuladosPageModule { }

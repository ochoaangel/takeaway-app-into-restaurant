import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EnPreparacionPage } from './en-preparacion.page';

describe('EnPreparacionPage', () => {
  let component: EnPreparacionPage;
  let fixture: ComponentFixture<EnPreparacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnPreparacionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EnPreparacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

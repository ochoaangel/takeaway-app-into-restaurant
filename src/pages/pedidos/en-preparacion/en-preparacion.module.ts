import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EnPreparacionPageRoutingModule } from './en-preparacion-routing.module';
import { EnPreparacionPage } from './en-preparacion.page';
import { MyComponentsModule } from 'src/app/components/my-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnPreparacionPageRoutingModule,
    MyComponentsModule
  ],
  declarations: [EnPreparacionPage]
})
export class EnPreparacionPageModule { }

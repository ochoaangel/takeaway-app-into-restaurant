import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EnPreparacionPage } from './en-preparacion.page';

const routes: Routes = [
  {
    path: '',
    component: EnPreparacionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EnPreparacionPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ReservasPageRoutingModule } from './reservas-routing.module';
import { ReservasPage } from './reservas.page';
import { MyComponentsModule } from 'src/app/components/my-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReservasPageRoutingModule,
    MyComponentsModule
  ],
  declarations: [ReservasPage]
})
export class ReservasPageModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MetodosDePagoPageRoutingModule } from './metodos-de-pago-routing.module';
import { MetodosDePagoPage } from './metodos-de-pago.page';
import { MyComponentsModule } from 'src/app/components/my-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MetodosDePagoPageRoutingModule,
    MyComponentsModule
  ],
  declarations: [MetodosDePagoPage]
})
export class MetodosDePagoPageModule { }

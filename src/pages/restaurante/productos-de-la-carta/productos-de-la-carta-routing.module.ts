import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductosDeLaCartaPage } from './productos-de-la-carta.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosDeLaCartaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductosDeLaCartaPageRoutingModule {}

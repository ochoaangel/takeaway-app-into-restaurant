import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ProductosDeLaCartaPageRoutingModule } from './productos-de-la-carta-routing.module';
import { ProductosDeLaCartaPage } from './productos-de-la-carta.page';
import { MyComponentsModule } from 'src/app/components/my-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductosDeLaCartaPageRoutingModule,
    MyComponentsModule
  ],
  declarations: [ProductosDeLaCartaPage]
})
export class ProductosDeLaCartaPageModule { }

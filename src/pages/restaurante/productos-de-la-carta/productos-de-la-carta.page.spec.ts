import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductosDeLaCartaPage } from './productos-de-la-carta.page';

describe('ProductosDeLaCartaPage', () => {
  let component: ProductosDeLaCartaPage;
  let fixture: ComponentFixture<ProductosDeLaCartaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductosDeLaCartaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductosDeLaCartaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

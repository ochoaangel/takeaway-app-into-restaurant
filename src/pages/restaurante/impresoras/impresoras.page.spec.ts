import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ImpresorasPage } from './impresoras.page';

describe('ImpresorasPage', () => {
  let component: ImpresorasPage;
  let fixture: ComponentFixture<ImpresorasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpresorasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ImpresorasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ImpresorasPageRoutingModule } from './impresoras-routing.module';
import { ImpresorasPage } from './impresoras.page';
import { MyComponentsModule } from 'src/app/components/my-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImpresorasPageRoutingModule,
    MyComponentsModule
  ],
  declarations: [ImpresorasPage]
})
export class ImpresorasPageModule { }
